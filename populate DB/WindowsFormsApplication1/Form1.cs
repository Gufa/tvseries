﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Store;
using VDS.RDF;
using VDS.RDF.Query.FullText;
using VDS.RDF.Query.FullText.Indexing;
using VDS.RDF.Query.FullText.Indexing.Lucene;
using VDS.RDF.Query.FullText.Schema;
using VDS.RDF.Query;
using VDS.RDF.Parsing;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Core.Objects;
using MySql.Data.MySqlClient;
using HtmlAgilityPack;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            string option = comboBoxOption.Text;
            //  backgroundWorker1.RunWorkerAsync(option);
            //getMoreInfo(1);
            //getMoreInfo();
            // getGenre(2);
            //getActors(2);
           // getCreators(2);
         //     getComposers(2);
          //  getIMDBImg();
            getNextEpisode();
           // getWikidataId();
            //getMoreInfoSeries();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                string option = (string)e.Argument;
                //MessageBox.Show(option);
                switch (option)
                {

                    case "getWikidataId":
                        MessageBox.Show(option);
                        getWikidataId();
                        break;
                    case "getMoreInfo1":
                        getMoreInfo(1);
                        break;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        public void getWikidataId()
        {
            //get all id series
            database db = new database(database.maindb);
            SparqlRemoteEndpoint endpoint = new SparqlRemoteEndpoint(new Uri("https://query.wikidata.org/sparql"), "https://query.wikidata.org");
            SparqlResultSet results = endpoint.QueryWithResultSet(@"
                                                                    PREFIX wd: <http://www.wikidata.org/entity/> 
                                                                    PREFIX wdt: <http://www.wikidata.org/prop/direct/>
                                                                    SELECT ?q WHERE {
                                                                      ?q wdt:P31 wd:Q5398426
                                                                    }"); //Q11424
            MySqlParameter wikidata = db.Command.Parameters.Add("?wikidata", MySqlDbType.String);
            foreach (SparqlResult result in results)
            {

                string wikidata_id = result["q"].ToString().Substring(result["q"].ToString().IndexOf("Q"));
                wikidata.Value = wikidata_id;

                db.ExecuteNonQuery("insert into series(wikidata_id) values(?wikidata)");
                // richTextBox1.Text += wikidata_id + "\n";
            }
            db.Close();
        }

        public void getMoreInfo(int caseid)
        {
            try
            {

                WebClient webClient = new WebClient();
                webClient.Headers[HttpRequestHeader.UserAgent] = "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)";
                List<string> ids = new List<string>();
                database db = new database(database.maindb);
                MySqlDataReader rd = db.ExecuteReader("select wikidata_id from movies");
                while (rd.Read())
                {
                    ids.Add(rd.GetString("wikidata_id"));
                }
                richTextBox1.Text = richTextBox1.Text + ids.Count;
                if (caseid == 1)
                {
                    string imdb = "", country = "", running_time = "", title = "", publication_date = "", wikipedia = "", movie, description = "", img_url = "", img = "", genreId = "", genre = "", genres = "";
                    SparqlRemoteEndpoint endpoint = new SparqlRemoteEndpoint(new Uri("https://query.wikidata.org/sparql"), "https://query.wikidata.org");
                    SparqlResultSet results = endpoint.QueryWithResultSet(@"PREFIX entity: <http://www.wikidata.org/entity/>
                                                                         PREFIX p: <http://www.wikidata.org/prop/direct/>
                                                                         PREFIX wdt: <http://www.wikidata.org/prop/direct/>
                                                                         PREFIX schema: <http://schema.org/>

                                                                         SELECT ?title ?publication_date ?article ?imdb ?country ?running_time WHERE {
                                                                         OPTIONAL { entity:Q21870042 rdfs:label ?title filter (lang(?title) = 'en'). } OPTIONAL { entity:Q21870042 wdt:P345 ?imdb.  }
                                                                         OPTIONAL { entity:Q21870042 wdt:P577 ?publication_date.  } 
                                                                         OPTIONAL { ?article schema:about entity:Q21870042 . ?article schema:inLanguage 'en' . 
                                                                         FILTER (SUBSTR(str(?article), 1, 25) = 'https://en.wikipedia.org/'). }
                                                                         OPTIONAL { entity:Q21870042 wdt:P495 ?countryID.
                                                                         ?countryID rdfs:label ?country filter (lang(?country) = 'en'). } OPTIONAL { entity:Q21870042 wdt:P2047 ?running_time. }}limit 1 ");


                    MySqlParameter wikidata = db.Command.Parameters.Add("?wikidata", MySqlDbType.String);
                    foreach (SparqlResult result in results)
                    {

                        title = result["title"].ToString();
                        title = title.Remove(title.Length - 3);
                        publication_date = result["publication_date"].ToString();
                        publication_date = publication_date.Remove(10);
                        wikipedia = result["article"].ToString();
                        wikipedia = wikipedia.Replace("%20", "_").Replace("%28", "(").Replace("%29", ")");
                        string[] words = wikipedia.Split('/');
                        movie = words[words.Length - 1];

                        try
                        { imdb = result["imdb"].ToString(); }
                        catch { }
                        try
                        {
                            country = result["country"].ToString();
                            country.Remove(country.Length - 3);
                        }
                        catch { }
                        try
                        {
                            running_time = result["running_time"].ToString();
                            running_time = running_time.Remove(3);
                        }
                        catch { }

                        db.AddParam("?title", title);
                        db.AddParam("?publication_date", publication_date);
                        db.AddParam("?imdb", imdb);
                        db.AddParam("?wikipedia", wikipedia);
                        db.AddParam("?origin_country", country);
                        if (running_time == "")
                            db.AddParam("?running_time", 0);
                        else db.AddParam("?running_time", Convert.ToInt16(running_time));



                        //db.ExecuteNonQuery("update movies set title=?title,publication_date=?publication_date,wikipedia=?wikipedia,imdb=?imdb,origin_country=?origin_country,running_time=?running_time  where wikidata_id='Q21870042'");
                        break;
                    }


                    //---------------------------------------------get DESCRIPTION AND IMAGE FROM WIKIPEDIA----------------------------------------------------------
                    string pagesource = webClient.DownloadString(wikipedia);
                    HtmlAgilityPack.HtmlDocument htmlDocument = new HtmlAgilityPack.HtmlDocument();
                    htmlDocument.LoadHtml(pagesource);
                    description = htmlDocument.DocumentNode.SelectSingleNode(("//div[@id='mw-content-text']//p[1]")).InnerText;
                    img_url = "http:" + htmlDocument.DocumentNode.SelectSingleNode(("//div[@id='mw-content-text']//table//img")).Attributes["src"].Value;
                    string[] split = img_url.Split('/');
                    img = split[split.Length - 1];
                    webClient.DownloadFileAsync(new Uri(img_url), "images/" + img);


                    //---------------------------------------------get GENRE------------------------------------------------------------------------------------------
                    results = endpoint.QueryWithResultSet(@"
                            PREFIX entity: <http://www.wikidata.org/entity/>
                            PREFIX wdt: <http://www.wikidata.org/prop/direct/>

                            SELECT ?genre ?genreID WHERE {
                            OPTIONAL { entity:Q21870007 wdt:P136 ?genreID.
                            ?genreID rdfs:label ?genre filter (lang(?genre) = 'en').} }");

                    MySqlParameter _genre = db.Command.Parameters.Add("?genre", MySqlDbType.String);
                    MySqlParameter _genreID = db.Command.Parameters.Add("?id", MySqlDbType.String);

                    foreach (SparqlResult result in results)
                    {
                        //daca nu avem genuri se duce pe catch
                        try
                        {
                            genre = result["genre"].ToString();
                            genre = genre.Remove(genre.Length - 3);
                            split = result["genreID"].ToString().Split('/');
                            genreId = split[split.Length - 1];
                            genres = genres + genreId + ",";
                            _genreID.Value = genreId;
                            rd = db.ExecuteReader("select * from genres where id=?id");
                            if (!rd.HasRows)
                            {
                                _genre.Value = genre;
                                db.ExecuteNonQuery("insert into genres(id,genre) values(?id,?genre)");
                            }
                        }
                        catch { }
                    }

                    //---------------------------------------------get Filming Location------------------------------------------------------------------------------------------


                    richTextBox1.Text = richTextBox1.Text + title + "\n" + publication_date + "\n" + wikipedia + "\n" + imdb + "\n" + country + "\n" + running_time + "\n" + description + "\n" + img + "\n" + genres + "\n";


                }
                db.Close();
            }
            catch (Exception e)
            {
                richTextBox1.Text = richTextBox1.Text + e.ToString();
            }
        }

        public void getMoreInfo()
        {
            try
            {
                long country_id = 0, network_id = 0;
                WebClient webClient = new WebClient();
                webClient.Headers[HttpRequestHeader.UserAgent] = "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)";
                List<string> ids = new List<string>();
                string title = "", imdb = "", wikipedia = "", language = "", network = "", list_episodes = "", start_time = "", country = "", date = "", movie = "", img = "";
                string description = "", number_of_episodes = "", nr_seasons = "", language_id = "";
                List<string> languages;


                database db = new database(database.maindb);
                MySqlDataReader rd = db.ExecuteReader("select wikidata_id from series");
                while (rd.Read())
                {
                    ids.Add(rd.GetString("wikidata_id"));
                }
                richTextBox1.Text = richTextBox1.Text + ids.Count;
                SparqlRemoteEndpoint endpoint = new SparqlRemoteEndpoint(new Uri("https://query.wikidata.org/sparql"), "https://query.wikidata.org");
                SparqlRemoteEndpoint endpoint2 = new SparqlRemoteEndpoint(new Uri("http://dbpedia.org/sparql"), "http://dbpedia.org");
                SparqlResultSet results;


                MySqlParameter wikidata_id = db.Command.Parameters.Add("?wikidata_id", MySqlDbType.String);
                MySqlParameter id_prm = db.Command.Parameters.Add("?id", MySqlDbType.String);
                MySqlParameter language_prm = db.Command.Parameters.Add("?language", MySqlDbType.String);
                MySqlParameter country_prm = db.Command.Parameters.Add("?country", MySqlDbType.String);
                MySqlParameter network_prm = db.Command.Parameters.Add("?network", MySqlDbType.String);
                MySqlParameter title_prm = db.Command.Parameters.Add("?title", MySqlDbType.String);
                MySqlParameter description_prm = db.Command.Parameters.Add("?description", MySqlDbType.String);
                MySqlParameter wikipedia_prm = db.Command.Parameters.Add("?wikipedia", MySqlDbType.String);
                MySqlParameter imdb_prm = db.Command.Parameters.Add("?imdb", MySqlDbType.String);
                MySqlParameter country_id_prm = db.Command.Parameters.Add("?country_id", MySqlDbType.Int64);
                MySqlParameter network_id_prm = db.Command.Parameters.Add("?network_id", MySqlDbType.Int64);
                MySqlParameter language_id_prm = db.Command.Parameters.Add("?language_id", MySqlDbType.String);
                MySqlParameter number_episodes_prm = db.Command.Parameters.Add("?nr_episodes", MySqlDbType.String);
                MySqlParameter nr_seasons_prm = db.Command.Parameters.Add("?nr_seasons", MySqlDbType.String);
                MySqlParameter start_date_prm = db.Command.Parameters.Add("?start_date", MySqlDbType.String);
                MySqlParameter img_prm = db.Command.Parameters.Add("?img", MySqlDbType.String);
                MySqlParameter link_episodes_prm = db.Command.Parameters.Add("?link_episodes", MySqlDbType.String);


                for (int i = 22981; i < 23021; i++)
                {
                    title = ""; imdb = ""; wikipedia = ""; country = ""; start_time = ""; network = ""; language = ""; list_episodes = ""; date = ""; movie = ""; description = ""; img = "";
                    number_of_episodes = ""; nr_seasons = "";
                    language_id = ""; country_id = 0; network_id = 0;
                    languages = new List<string>();
                    string id = ids[i];
                    richTextBox1.Text += id + "\n";
                    //    if (id == "Q16994089") { richTextBox1.Text += "YEEEEEEES" + i; break; }
                    string query = @"PREFIX entity: <http://www.wikidata.org/entity/>
                                     PREFIX p: <http://www.wikidata.org/prop/direct/>
                                     PREFIX wdt: <http://www.wikidata.org/prop/direct/>
                                     PREFIX schema: <http://schema.org/>

                                     SELECT ?title ?article ?imdb ?country ?original_language ?original_network ?list_episodes ?start_time ?date WHERE {
                                     OPTIONAL { entity:" + id + "  rdfs:label ?title filter (lang(?title) = 'en'). } OPTIONAL { entity:" + id + "   wdt:P345 ?imdb.  }" +
                                     "OPTIONAL { ?article schema:about entity:" + id + "   . ?article schema:inLanguage 'en' . FILTER (SUBSTR(str(?article), 1, 25) = 'https://en.wikipedia.org/'). }" +
                                     "OPTIONAL { entity:" + id + "   wdt:P495 ?countryID." +
                                     "?countryID rdfs:label ?country filter (lang(?country) = 'en'). }" +
                                     "OPTIONAL { entity:" + id + "   wdt:P364 ?original_languageID." +
                                     "?original_languageID rdfs:label ?original_language filter (lang(?original_language) = 'en'). } " +
                                     "OPTIONAL {" +
                                       "entity:" + id + "   wdt:P449 ?original_networkID." +
                                      "?original_networkID rdfs:label ?original_network filter(lang(?original_network) = 'en'). }" +
                                     "OPTIONAL { entity:" + id + "   wdt:P1811 ?list_episodes. }" +
                                     "OPTIONAL { entity:" + id + "   wdt:P580 ?start_time. }" +
                                     "OPTIONAL { entity:" + id + "   wdt:P577 ?date. } }";
                    //richTextBox1.Text += query+"\n";


                    //get results from wikidata
                    results = endpoint.QueryWithResultSet(query);
                    foreach (SparqlResult result in results)
                    {
                        try
                        { imdb = result["imdb"].ToString(); }
                        catch { }
                        try
                        {
                            title = result["title"].ToString().Trim();
                            title = title.Remove(title.Length - 3);
                        }
                        catch { }
                        try
                        {
                            wikipedia = result["article"].ToString().Trim();
                            wikipedia = wikipedia.Replace("%20", "_").Replace("%28", "(").Replace("%29", ")").Replace("%C3%89", "É").Replace("'", "%27")
                                .Replace("%3A", ":").Replace("%26", "&").Replace("%24", "$").Replace("%2A", "*").Replace("%21", "!").Replace("%2C", ",").Replace("%C3%AE", "î");
                            string[] words = wikipedia.Split('/');
                            movie = words[words.Length - 1].Trim();
                            if (movie.Equals("Kikoriki")) movie = "Kikoriki_(animated_series)";
                        }
                        catch { }
                        try
                        {
                            country = result["country"].ToString().Trim();
                            string[] words = country.Split('|');
                            if (words.Length == 1)
                                country = words[0].Remove(words[0].Length - 3).Trim();
                            else country = words[0].Trim();
                        }
                        catch { }
                        try
                        {
                            language = result["original_language"].ToString().Trim();
                            language = language.Remove(language.Length - 3);
                            if (language.Contains("/"))
                            {
                                string[] split = language.Split('/');
                                int index = split[split.Length - 1].IndexOf('_');
                                language = split[split.Length - 1].Remove(index);
                            }

                        }
                        catch { }
                        try
                        {
                            network = result["original_network"].ToString().Trim();
                            network = network.Remove(network.Length - 3);

                        }
                        catch { }
                        try
                        {
                            list_episodes = result["list_episodes"].ToString().Trim();
                            string[] split = list_episodes.Split('/');
                            list_episodes = split[split.Length - 1].Trim();
                        }
                        catch { }
                        try
                        {
                            start_time = result["start_time"].ToString().Trim();
                            start_time = start_time.Remove(10);
                        }
                        catch { }
                        try
                        {
                            date = result["date"].ToString().Trim();
                            date = date.Remove(10);
                        }
                        catch { }
                    }

                    if (start_time == "") start_time = date;

                    //get results from dbpedia
                    string resource = "http://dbpedia.org/resource/" + movie;
                    query = "SELECT ?description ?img ?nr_episodes ?nr_seasons ?channel ?release_date ?language ?country ?network ?numEpisodes ?firstAired " +
                            "WHERE { <" + resource + "> <http://dbpedia.org/ontology/abstract> ?description." +
                            " OPTIONAL { <" + resource + "> <http://dbpedia.org/ontology/thumbnail> ?img. }" +
                            " OPTIONAL { <" + resource + "> <http://dbpedia.org/ontology/numberOfEpisodes> ?nr_episodes. }" +
                            " OPTIONAL { <" + resource + "> <http://dbpedia.org/ontology/numberOfSeasons> ?nr_seasons. }" +
                           " OPTIONAL { <" + resource + "> <http://dbpedia.org/property/numEpisodes> ?numEpisodes.  } " +
                          " OPTIONAL { <" + resource + "> <http://dbpedia.org/property/firstAired> ?firstAired.  } " +
                            " OPTIONAL { <" + resource + "> <http://dbpedia.org/ontology/channel> ?channelID." +
                            " ?channelID <http://dbpedia.org/ontology/alias> ?channel. }" +
                            " OPTIONAL { <" + resource + "> <http://dbpedia.org/property/network> ?network.  } " +
                            " OPTIONAL { <" + resource + "> <http://dbpedia.org/ontology/releaseDate> ?release_date. }" +
                           " OPTIONAL { <" + resource + "> <http://dbpedia.org/property/language> ?language. }" +

                           " OPTIONAL { <" + resource + "> <http://dbpedia.org/ontology/country> ?country. }" +
                            "filter(langMatches(lang(?description), 'EN'))} ORDER BY ?value LIMIT 1";
                    //  richTextBox1.Text = richTextBox1.Text + query+"\n";
                    results = endpoint2.QueryWithResultSet(query);
                    foreach (SparqlResult result in results)
                    {
                        description = result["description"].ToString();
                        description = description.Remove(description.Length - 3);
                        try
                        { img = result["img"].ToString(); }
                        catch { }
                        try
                        {
                            number_of_episodes = result["nr_episodes"].ToString();
                            int index = number_of_episodes.IndexOf('^');
                            number_of_episodes = number_of_episodes.Remove(index).Trim().Replace("@en", "");
                        }
                        catch { }

                        try
                        {
                            number_of_episodes = result["numEpisodes"].ToString();
                            int index = number_of_episodes.IndexOf('^');
                            number_of_episodes = number_of_episodes.Remove(index).Trim().Replace("@en", "");
                        }
                        catch { }

                        try
                        {
                            nr_seasons = result["nr_seasons"].ToString();
                            int index = nr_seasons.IndexOf('^');
                            nr_seasons = nr_seasons.Remove(index);
                        }
                        catch { }
                        if (network == "")
                            try
                            {
                                network = result["channel"].ToString();
                                network = network.Trim();
                                if (network.Contains("/"))
                                {
                                    string[] split = network.Split('/');
                                    network = split[split.Length - 1].Replace("_", " ");
                                }
                            }
                            catch { }
                        if (network == "")
                            try
                            {
                                network = result["network"].ToString();
                                network = network.Remove(network.Length - 3);
                                if (network.Contains("/"))
                                {
                                    string[] split = network.Split('/');
                                    network = split[split.Length - 1].Replace("_", " ");
                                }
                            }
                            catch { }
                        if (start_time == "")
                            try
                            {
                                start_time = result["release_date"].ToString();
                                start_time = start_time.Remove(10);

                            }
                            catch { }
                        if (start_time == "")
                            try
                            {
                                start_time = result["firstAired"].ToString();

                                int index = start_time.IndexOf('^');
                                start_time = start_time.Remove(index).Trim();

                                //start_time = start_time.Remove(10);
                            }
                            catch { start_time = ""; }
                        if (language == "")
                            try
                            {
                                language = result["language"].ToString().Replace("@en", "");
                                if (language.Contains("/"))
                                {
                                    string[] split = language.Split('/');
                                    int index = split[split.Length - 1].IndexOf('_');
                                    if (index != -1) language = split[split.Length - 1].Remove(index);
                                    else language = split[split.Length - 1];
                                }
                                if (language.Contains(","))
                                {
                                    string[] split = language.Split(',');
                                    for (int j = 0; j < split.Length; j++)
                                    {
                                        languages.Add(split[j].Trim());
                                    }
                                }
                            }
                            catch { }
                        if (country == "")
                            try
                            {
                                country = result["country"].ToString();
                                if (country.Contains("/"))
                                {
                                    string[] split = country.Split('/');
                                    country = split[split.Length - 1];
                                }
                            }
                            catch { }


                    }
                    string img_name = "";


                    //download img from html wikipedia code

                    richTextBox1.Text += wikipedia + "\n";
                    string pagesource = webClient.DownloadString(wikipedia);
                    HtmlAgilityPack.HtmlDocument htmlDocument = new HtmlAgilityPack.HtmlDocument();
                    htmlDocument.LoadHtml(pagesource);
                    try

                    {
                        if (img == "") img = "http:" + htmlDocument.DocumentNode.SelectSingleNode(("//div[@id='mw-content-text']//table[@class='infobox vevent']//img")).Attributes["src"].Value;
                    }
                    catch { }

                    Dictionary<string, List<string>> info = new Dictionary<string, List<string>>();
                    if (description == "")
                    {
                        description = htmlDocument.DocumentNode.SelectSingleNode(("//div[@id='mw-content-text']//p")).InnerText;
                    }
                    if (htmlDocument.DocumentNode.SelectNodes("//div[@id='mw-content-text']//table[@class='infobox vevent']") != null)
                    {
                        foreach (HtmlNode htmlNode in (IEnumerable<HtmlNode>)htmlDocument.DocumentNode.SelectNodes("//div[@id='mw-content-text']//table[@class='infobox vevent']//tr"))
                        {
                            string[] lines = htmlNode.InnerText.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
                            if (lines[1] != "")
                            {
                                List<string> inform = new List<string>();
                                for (int j = 2; j < lines.Length; j++)
                                    inform.Add(lines[j]);
                                if (!info.ContainsKey(lines[1])) info.Add(lines[1], inform);
                                // richTextBox1.Text += lines[1] + "\n";
                            }

                        }
                        if (country == "") if (info.ContainsKey("Country of origin")) country = info["Country of origin"][0];
                        if (language == "") if (info.ContainsKey("Original language(s)")) language = info["Original language(s)"][0];
                        if (number_of_episodes == "") if (info.ContainsKey("No. of episodes")) number_of_episodes = info["No. of episodes"][0];
                        if (nr_seasons == "") if (info.ContainsKey("No. of seasons")) nr_seasons = info["No. of seasons"][0];
                        if (start_time == "")
                            if (info.ContainsKey("Original release"))
                            {
                                string release = info["Original release"][0];
                                if (release.Contains("First")) release = info["Original release"][1];
                                if (release.Contains("Present"))
                                {
                                    int index = release.IndexOf('&');
                                    release = release.Remove(index).Trim();
                                    start_time = release;
                                }
                                else
                                {
                                    try
                                    {
                                        start_time = release.Split('(', ')')[1];
                                    }
                                    catch
                                    {
                                        int index = release.IndexOf('&');
                                        if (index != -1) release = release.Remove(index).Trim();
                                        start_time = release;
                                    }
                                }
                            }
                    }


                    if (img != "")
                    {
                        string[] split = img.Split('/');
                        img_name = split[split.Length - 1].Replace("?width=300", "");
                        if (img_name.Contains("Question_book"))
                        {
                            img_name = "";
                            img = "";
                        }
                        else
                            try
                            {
                                webClient.DownloadFile(new Uri(img), "images/" + img_name);
                            }
                            catch { }

                    }

                    //get link of list of episodes from wikipedia
                    if (list_episodes != "")
                    {
                        query = @"PREFIX entity: <http://www.wikidata.org/entity/>
                            prefix schema: <http://schema.org/>
                            SELECT ?article WHERE {
                                   ?article schema:about entity:" + list_episodes + "." +
                                    " ?article schema:inLanguage 'en'." +
                                    " FILTER(SUBSTR(str(?article), 1, 25) = 'https://en.wikipedia.org/') }";
                        //  richTextBox1.Text += query;
                        results = endpoint.QueryWithResultSet(query);
                        foreach (SparqlResult result in results)
                        {
                            list_episodes = result["article"].ToString();
                        }
                    }




                    //start inserting in database
                    if (language != "")
                    {
                        if (languages.Count > 0)
                            for (int j = 0; j < languages.Count; j++)
                            {
                                language_prm.Value = languages[j];
                                rd = db.ExecuteReader("select id from languages where language=?language");
                                if (rd.HasRows)
                                    while (rd.Read())
                                        language_id += rd.GetInt32("id") + ",";
                                else
                                {
                                    language_id += db.ExecuteNonQuery("insert into languages(language) values(?language)").ToString() + ",";
                                }
                            }
                        else
                        {
                            language_prm.Value = language;
                            rd = db.ExecuteReader("select id from languages where language=?language");
                            if (rd.HasRows)
                                while (rd.Read())
                                    language_id = rd.GetInt32("id").ToString();
                            else
                            {
                                language_id = db.ExecuteNonQuery("insert into languages(language) values(?language)").ToString();
                            }
                        }
                    }
                    if (country != "")
                    {
                        country_prm.Value = country;
                        rd = db.ExecuteReader("select id from countries where country=?country");
                        if (rd.HasRows)
                            while (rd.Read())
                                country_id = rd.GetInt32("id");
                        else
                        {
                            country_id = db.ExecuteNonQuery("insert into countries(country) values(?country)");
                        }
                    }
                    if (network != "")
                    {
                        network_prm.Value = network;
                        rd = db.ExecuteReader("select id from network where network=?network");
                        if (rd.HasRows)
                            while (rd.Read())
                                network_id = rd.GetInt32("id");
                        else
                        {
                            network_id = db.ExecuteNonQuery("insert into network(network) values(?network)");
                        }
                    }

                    richTextBox1.Text = richTextBox1.Text + "i:" + i + "ID:" + id + "\nTitle: " + title + "\nImg: " + img + "\nImg name:" + img_name + "\nDescription: " + description + "\nWikipedia: " + wikipedia + "\nIMDB: " + imdb + "\nCountry: " + country + "\nLanguage: " + language + "\nLanguage id: " + language_id + "\nNetwork: " +
              network + "\nList_episodes: " + list_episodes + "\nDate: " + start_time + "\nMovie: " + movie + "\nNr episodes:" + number_of_episodes + "\nNr seasons:" + nr_seasons + "\n\n";

                    id_prm.Value = id;
                    title_prm.Value = title;
                    description_prm.Value = description;
                    wikipedia_prm.Value = wikipedia;
                    imdb_prm.Value = imdb;
                    country_id_prm.Value = country_id;
                    language_id_prm.Value = language_id;
                    network_id_prm.Value = network_id;
                    number_episodes_prm.Value = number_of_episodes;
                    nr_seasons_prm.Value = nr_seasons;
                    start_date_prm.Value = start_time;
                    img_prm.Value = img_name;
                    link_episodes_prm.Value = list_episodes;

                    db.ExecuteNonQuery(@"update series set title=?title,description=?description,wikipedia=?wikipedia,imdb=?imdb,country_id=?country_id,language_id=?language_id,network_id=?network_id 
                              ,number_episodes=?nr_episodes,number_seasons=?nr_seasons,start_date=?start_date,img=?img,link_episodes=?link_episodes where wikidata_id=?id");





                }

            }
            catch (Exception e)
            { richTextBox1.Text += e.ToString(); }
        }

        public void getGenre(int option)
        {
            //option 1 for wikidata, option 2 for wikipedia
            try
            {
                database db = new database(database.maindb);
                List<string> ids = new List<string>();
                Dictionary<string, string> ids_wikipedia = new Dictionary<string, string>();
                SparqlResultSet results;
                List<string> genres = new List<string>();
                string genre_ids = "";
                string genre = "";

                if (option == 1)
                {
                    MySqlDataReader rd = db.ExecuteReader("select wikidata_id from series");
                    while (rd.Read())
                    {
                        ids.Add(rd.GetString("wikidata_id"));
                    }
                    SparqlRemoteEndpoint endpoint = new SparqlRemoteEndpoint(new Uri("https://query.wikidata.org/sparql"), "https://query.wikidata.org");
                    MySqlParameter wikidata_id = db.Command.Parameters.Add("?wikidata_id", MySqlDbType.String);
                    MySqlParameter genrePrm = db.Command.Parameters.Add("?genre", MySqlDbType.String);
                    MySqlParameter genre_idsPrm = db.Command.Parameters.Add("?genre_ids", MySqlDbType.String);

                    richTextBox1.Text += ids.Count;
                    for (int i = 17000; i < 23020; i++)
                    {
                        richTextBox1.Text += i + " , " + ids[i] + "\n";
                        genre_ids = "";
                        string query = @"PREFIX entity: <http://www.wikidata.org/entity/>
                                    PREFIX wdt: <http://www.wikidata.org/prop/direct/>
                                    SELECT ?genre WHERE {
                                    OPTIONAL { entity:" + ids[i] + " wdt:P136 ?genreID." +
                                        " ?genreID rdfs:label ?genre. filter(lang(?genre) = 'en').       } }";
                        // richTextBox1.Text += query;
                        results = endpoint.QueryWithResultSet(query);
                        foreach (SparqlResult result in results)
                        {
                            genre = "";
                            try
                            {
                                genre = result["genre"].ToString();
                                genre = genre.Remove(genre.Length - 3);
                            }
                            catch { }
                            if (genre != "")
                            {
                                richTextBox1.Text += genre + "\n";
                                genrePrm.Value = genre.Trim();

                                string[] split_genre = genre.Split(new string[] { "and" }, StringSplitOptions.None);
                                if (split_genre.Length > 0)
                                    for (int j = 0; j < split_genre.Length; j++)
                                    {
                                        genrePrm.Value = split_genre[j].Trim();
                                        rd = db.ExecuteReader("select id from genres where lower(genre)=lower(?genre)");
                                        if (rd.HasRows)
                                            while (rd.Read())
                                                genre_ids += rd.GetInt32("id") + ",";
                                        else
                                        {
                                            genre_ids += db.ExecuteNonQuery("insert into genres(genre) values(?genre)").ToString() + ",";
                                        }
                                    }
                                else
                                {
                                    rd = db.ExecuteReader("select id from genres where lower(genre)=lower(?genre)");
                                    if (rd.HasRows)
                                        while (rd.Read())
                                            genre_ids += rd.GetInt32("id") + ",";
                                    else
                                    {
                                        genre_ids += db.ExecuteNonQuery("insert into genres(genre) values(?genre)").ToString() + ",";
                                    }
                                }
                            }
                        }
                        if (genre_ids != "")
                        {
                            richTextBox1.Text += genre_ids + "\n";
                            genre_idsPrm.Value = genre_ids;
                            wikidata_id.Value = ids[i];
                            db.ExecuteNonQuery("update series set genre_id=?genre_ids where wikidata_id=?wikidata_id");
                        }
                    }
                }
                else
                {
                    int count = 0;
                    MySqlDataReader rd = db.ExecuteReader("select wikidata_id,wikipedia from series where genre_id='' and id>26028");
                    while (rd.Read())
                    {
                        ids_wikipedia.Add(rd.GetString("wikidata_id"), rd.GetString("wikipedia"));
                    }
                    SparqlRemoteEndpoint endpoint = new SparqlRemoteEndpoint(new Uri("http://dbpedia.org/sparql"), "http://dbpedia.org");
                    MySqlParameter wikidata_id = db.Command.Parameters.Add("?wikidata_id", MySqlDbType.String);
                    MySqlParameter genrePrm = db.Command.Parameters.Add("?genre", MySqlDbType.String);
                    MySqlParameter genre_idsPrm = db.Command.Parameters.Add("?genre_ids", MySqlDbType.String);

                    richTextBox1.Text += ids_wikipedia.Count + "\n";
                    foreach (KeyValuePair<string, string> serie in ids_wikipedia.ToList())
                    {
                        genre_ids = "";
                        count++;

                        if (count > 0)
                        {
                            string id = serie.Key;
                            string wikipedia = serie.Value;
                            richTextBox1.Text += count + " , " + id + "\n" + wikipedia + "\n";
                            genre_ids = "";

                            string[] words = serie.Value.Split('/');
                            string movie = words[words.Length - 1].Trim();
                            if (movie.Equals("Kikoriki")) movie = "Kikoriki_(animated_series)";
                            string query = @"SELECT ?genre WHERE { " +
                                            "<http://dbpedia.org/resource/" + movie + "> <http://dbpedia.org/ontology/genre> ?genreID ." +
                                            " ?genreID rdfs:label ?genre." +
                                            " filter(langMatches(lang(?genre), 'EN')) }";
                            // richTextBox1.Text += query;
                            results = endpoint.QueryWithResultSet(query);
                            foreach (SparqlResult result in results)
                            {

                                genre = result["genre"].ToString().Remove(result["genre"].ToString().Length - 3).Trim();
                                genrePrm.Value = genre;
                                richTextBox1.Text += genre + "\n";
                                rd = db.ExecuteReader("select id from genres where lower(genre)=lower(?genre)");
                                if (rd.HasRows)
                                    while (rd.Read())
                                        genre_ids += rd.GetInt32("id") + ",";
                                else
                                {
                                    genre_ids += db.ExecuteNonQuery("insert into genres(genre) values(?genre)").ToString() + ",";
                                }
                            }

                            //in caz de nu avem genuri gasit pe dbpedia citim codul html de pe wikipedia si luam genul de acolo
                            if (genre_ids == "")
                            {
                                WebClient webClient = new WebClient();
                                webClient.Headers[HttpRequestHeader.UserAgent] = "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)";
                                string pagesource = webClient.DownloadString(wikipedia);
                                HtmlAgilityPack.HtmlDocument htmlDocument = new HtmlAgilityPack.HtmlDocument();
                                htmlDocument.LoadHtml(pagesource);
                                genres = new List<string>();
                                if (htmlDocument.DocumentNode.SelectNodes("//div[@id='mw-content-text']//table[@class='infobox vevent']") != null)
                                {
                                    int nr = 0;
                                    foreach (HtmlNode htmlNode in (IEnumerable<HtmlNode>)htmlDocument.DocumentNode.SelectNodes("//div[@id='mw-content-text']//table[@class='infobox vevent']//tr"))
                                    {
                                        nr++;
                                        string[] lines = htmlNode.InnerText.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
                                        if (lines[1] == "Genre")
                                        {
                                            for (int j = 2; j < lines.Length; j++)
                                            {
                                                if (lines[j].Trim() != "" && !lines[j].Contains("["))
                                                    genres.Add(lines[j].Trim());

                                            }
                                        }
                                        if (nr == 3) break;
                                    }
                                    if (genres.Count > 0)
                                    {
                                        richTextBox1.Text += "Wikipedia:\n";
                                        foreach (string gen in genres)
                                        {
                                            //in caz de avem mai multe genuri cu , intre ele
                                            if (gen.Contains(",") || gen.Contains("/"))
                                            {
                                                string[] split;
                                                if (gen.Contains(","))
                                                    split = gen.Split(',');
                                                else split = gen.Split('/');
                                                foreach (string word in split)
                                                {
                                                    richTextBox1.Text += word + "\n";
                                                    genrePrm.Value = word.Trim();
                                                    rd = db.ExecuteReader("select id from genres where lower(genre)=lower(?genre)");
                                                    if (rd.HasRows)
                                                        while (rd.Read())
                                                            genre_ids += rd.GetInt32("id") + ",";
                                                    else
                                                    {
                                                        genre_ids += db.ExecuteNonQuery("insert into genres(genre) values(?genre)").ToString() + ",";
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                richTextBox1.Text += gen + "\n";
                                                genrePrm.Value = gen.Trim();
                                                rd = db.ExecuteReader("select id from genres where lower(genre)=lower(?genre)");
                                                if (rd.HasRows)
                                                    while (rd.Read())
                                                        genre_ids += rd.GetInt32("id") + ",";
                                                else
                                                {
                                                    genre_ids += db.ExecuteNonQuery("insert into genres(genre) values(?genre)").ToString() + ",";
                                                }
                                            }
                                        }

                                    }

                                }
                            }
                            if (genre_ids != "")
                            {
                                richTextBox1.Text += genre_ids + "\n";
                                genre_idsPrm.Value = genre_ids;
                                wikidata_id.Value = id;
                                db.ExecuteNonQuery("update series set genre_id=?genre_ids where wikidata_id=?wikidata_id");

                            }
                        }
                        if (count == 2000) break;
                    }
                }

                db.Close();
            }
            catch (Exception e)
            {
                richTextBox1.Text += richTextBox1.Text + e.ToString();
            }
        }

        public void getActors(int option)
        {
            try
            {
                // option 1 get id and title from wikidata
                // option 2 get more info about actors from wikidata
                database db = new database(database.maindb);
                List<string> ids = new List<string>();
                Dictionary<string, string> ids_wikipedia = new Dictionary<string, string>();
                SparqlResultSet results;
                List<string> genres = new List<string>();
                string actor_ids = "";
                string actor = "";
                string wiki_id = "";

                if (option == 1)
                {
                    MySqlDataReader rd = db.ExecuteReader("select wikidata_id from series");
                    while (rd.Read())
                    {
                        ids.Add(rd.GetString("wikidata_id"));
                    }
                    SparqlRemoteEndpoint endpoint = new SparqlRemoteEndpoint(new Uri("https://query.wikidata.org/sparql"), "https://query.wikidata.org");
                    MySqlParameter serie_id = db.Command.Parameters.Add("?serie_id", MySqlDbType.String);
                    MySqlParameter actor_id = db.Command.Parameters.Add("?actor_id", MySqlDbType.String);
                    MySqlParameter actorPrm = db.Command.Parameters.Add("?actor", MySqlDbType.String);
                    MySqlParameter actor_idsPrm = db.Command.Parameters.Add("?actor_ids", MySqlDbType.String);

                    richTextBox1.Text += ids.Count;

                    for (int i = 20000; i < ids.Count; i++)
                    {
                        if (ids[i] == "Q660263")
                        {
                            richTextBox1.Text += "Here:" + i + "\n";
                            //   break; 
                        }
                        richTextBox1.Text += i + " , " + ids[i] + "\n";
                        actor_ids = "";
                        string query = @"PREFIX entity: <http://www.wikidata.org/entity/>
                                    PREFIX wdt: <http://www.wikidata.org/prop/direct/>
                                    SELECT ?actorID ?actor WHERE {
                                    OPTIONAL { entity:" + ids[i] + " wdt:P161 ?actorID." +
                                        " ?actorID rdfs:label ?actor. filter(lang(?actor) = 'en').       } }";
                        //richTextBox1.Text += query;
                        results = endpoint.QueryWithResultSet(query);
                        foreach (SparqlResult result in results)
                        {
                            actor = "";
                            wiki_id = "";
                            try
                            {
                                actor = result["actor"].ToString();
                                actor = actor.Remove(actor.Length - 3).Trim();
                                wiki_id = result["actorID"].ToString().Substring(result["actorID"].ToString().IndexOf("Q")).Trim();

                            }
                            catch { }
                            if (actor != "")
                            {
                                richTextBox1.Text += actor + "\n" + wiki_id + "\n";
                                actorPrm.Value = actor;
                                actor_id.Value = wiki_id;
                                rd = db.ExecuteReader("select id from actors where wikidata_id=?actor_id");
                                if (rd.HasRows)
                                    while (rd.Read())
                                    {
                                        actor_ids += rd.GetInt32("id") + ",";
                                    }
                                else
                                {
                                    actor_ids += db.ExecuteNonQuery("insert into actors(wikidata_id,name) values(?actor_id,?actor)").ToString() + ",";
                                }
                            }

                        }
                        if (actor_ids != "")
                        {
                            richTextBox1.Text += actor_ids + "\n";
                            actor_idsPrm.Value = actor_ids;
                            serie_id.Value = ids[i];
                            db.ExecuteNonQuery("update series set actor_ids=?actor_ids where wikidata_id=?serie_id");
                        }
                    }
                }
                //get more info from wikidata about actors
                else if (option == 2)
                {
                    WebClient webClient = new WebClient();
                    string gender, image, date_of_birth, date_of_death, place_birth, subtitle, imdb, wikipedia, educated, education_ids, award, awards_ids, description;
                    MySqlDataReader rd = db.ExecuteReader("select wikidata_id from actors");
                    while (rd.Read())
                    {
                        ids.Add(rd.GetString("wikidata_id"));
                    }
                    SparqlRemoteEndpoint endpoint = new SparqlRemoteEndpoint(new Uri("https://query.wikidata.org/sparql"), "https://query.wikidata.org");
                    SparqlRemoteEndpoint endpoint2 = new SparqlRemoteEndpoint(new Uri("http://dbpedia.org/sparql"), "http://dbpedia.org");
                    MySqlParameter idPrm = db.Command.Parameters.Add("?wikidata_id", MySqlDbType.String);
                    MySqlParameter actor_id = db.Command.Parameters.Add("?actor_id", MySqlDbType.String);
                    MySqlParameter universityPrm = db.Command.Parameters.Add("?university", MySqlDbType.String);
                    MySqlParameter university_idsPrm = db.Command.Parameters.Add("?university_ids", MySqlDbType.String);
                    MySqlParameter awardPrm = db.Command.Parameters.Add("?award", MySqlDbType.String);
                    MySqlParameter award_idsPrm = db.Command.Parameters.Add("?awards_id", MySqlDbType.String);
                    MySqlParameter descriptionPrm = db.Command.Parameters.Add("?description", MySqlDbType.String);
                    MySqlParameter genderPrm = db.Command.Parameters.Add("?gender", MySqlDbType.Int16);
                    MySqlParameter dateBirthPrm = db.Command.Parameters.Add("?date_of_birth", MySqlDbType.String);
                    MySqlParameter dateDeathPrm = db.Command.Parameters.Add("?date_of_death", MySqlDbType.String);
                    MySqlParameter place_birthPrm = db.Command.Parameters.Add("?place_birth", MySqlDbType.String);
                    MySqlParameter subtitlePrm = db.Command.Parameters.Add("?subtitle", MySqlDbType.String);
                    MySqlParameter imdbPrm = db.Command.Parameters.Add("?imdb", MySqlDbType.String);
                    MySqlParameter wikipediaPrm = db.Command.Parameters.Add("?wikipedia", MySqlDbType.String);
                    MySqlParameter imgPrm = db.Command.Parameters.Add("?img", MySqlDbType.String);



                    richTextBox1.Text += ids.Count;
                    for (int i = 12040; i < 12500; i++)
                    {
                        gender = ""; image = ""; date_of_birth = ""; date_of_death = ""; place_birth = ""; subtitle = ""; imdb = ""; wikipedia = ""; educated = ""; education_ids = "";
                        award = ""; awards_ids = ""; description = "";
                        richTextBox1.Text += i + " , " + ids[i] + "\n";
                        string query = @"PREFIX entity: <http://www.wikidata.org/entity/>
                                        PREFIX wdt: <http://www.wikidata.org/prop/direct/>
                                        PREFIX schema: <http://schema.org/>

                                        SELECT ?gender ?image ?date_of_birth ?date_of_death ?place_birth ?subtitle ?imdb ?article WHERE {
                                        OPTIONAL { entity:" + ids[i] + " schema:description ?subtitle filter (lang(?subtitle) = 'en'). } " +
                                        "OPTIONAL { entity:" + ids[i] + " wdt:P21 ?genderID. " +
                                       " ?genderID rdfs:label ?gender filter(lang(?gender) = 'en').  } " +
                                       " OPTIONAL { entity:" + ids[i] + " wdt:P18 ?image.  } " +
                                      "  OPTIONAL { entity:" + ids[i] + " wdt:P569 ?date_of_birth.  } " +
                                       " OPTIONAL { entity:" + ids[i] + " wdt:P570 ?date_of_death.  } " +
                                      "  OPTIONAL { entity:" + ids[i] + " wdt:P19 ?place_birthID. " +
                                      "  ?place_birthID rdfs:label ?place_birth filter(lang(?place_birth) = 'en').   } " +
                                      "  OPTIONAL { entity:" + ids[i] + " wdt:P345 ?imdb.  } " +
                                      "  OPTIONAL { ?article schema:about entity:" + ids[i] + " . ?article schema:inLanguage 'en' . FILTER (SUBSTR(str(?article), 1, 25) = 'https://en.wikipedia.org/'). } }";
                        // richTextBox1.Text += query+"\n\n";
                        results = endpoint.QueryWithResultSet(query);
                        foreach (SparqlResult result in results)
                        {
                            try { gender = result["gender"].ToString().Remove(result["gender"].ToString().Length - 3).Trim(); }
                            catch { }
                            try { image = result["image"].ToString().Trim().Replace("%20", "_").Replace("%28", "(").Replace("%29", ")"); }
                            catch { }
                            try { date_of_birth = result["date_of_birth"].ToString().Remove(10).Trim(); }
                            catch { }
                            try { date_of_death = result["date_of_death"].ToString().Remove(10).Trim(); }
                            catch { }
                            try { place_birth = result["place_birth"].ToString().Remove(result["place_birth"].ToString().Length - 3).Trim(); }
                            catch { }
                            try { subtitle = result["subtitle"].ToString().Remove(result["subtitle"].ToString().Length - 3).Trim(); }
                            catch { }
                            try { imdb = result["imdb"].ToString().Trim(); }
                            catch { }
                            try
                            {
                                wikipedia = result["article"].ToString().Trim();
                                wikipedia = wikipedia.Replace("%20", "_").Replace("%28", "(").Replace("%29", ")").Replace("%C3%89", "É").Replace("'", "%27")
                               .Replace("%3A", ":").Replace("%26", "&").Replace("%24", "$").Replace("%2A", "*").Replace("%21", "!").Replace("%2C", ",").Replace("%C3%AE", "î");
                            }
                            catch { }
                        }
                        richTextBox1.Text += "Subtitle: " + subtitle + "\nGender: " + gender + "\n" + "Image: " + image + "\nDate of birth: " + date_of_birth + "\nDate of death: " + date_of_death +
                            "\nPlace birth: " + place_birth + "\nImdb: " + imdb + "\nWikipedia: " + wikipedia + "\nEducated:";

                        //get College
                        query = @"PREFIX entity: <http://www.wikidata.org/entity/>
                                        PREFIX wdt: <http://www.wikidata.org/prop/direct/>
                                        SELECT ?educated  WHERE {
                                        OPTIONAL { entity:" + ids[i] + " wdt:P69 ?educatedID. " +
                                      " ?educatedID rdfs:label ?educated filter(lang(?educated) = 'en').  } }";
                        results = endpoint.QueryWithResultSet(query);
                        foreach (SparqlResult result in results)
                        {
                            educated = "";
                            try { educated = result["educated"].ToString().Remove(result["educated"].ToString().Length - 3).Trim(); }
                            catch { }
                            if (educated != "")
                            {
                                richTextBox1.Text += educated + "\n";
                                universityPrm.Value = educated;
                                rd = db.ExecuteReader("select id from education where lower(name)=lower(?university)");
                                if (rd.HasRows)
                                    while (rd.Read())
                                    {
                                        education_ids += rd.GetInt32("id") + ",";
                                    }
                                else
                                {
                                    education_ids += db.ExecuteNonQuery("insert into education(name) values(?university)").ToString() + ",";
                                }
                            }
                        }
                        richTextBox1.Text += "\nEducation ids: " + education_ids + "\nAwards: ";

                        //get Awards
                        query = @"PREFIX entity: <http://www.wikidata.org/entity/>
                                        PREFIX wdt: <http://www.wikidata.org/prop/direct/>
                                        SELECT ?award  WHERE {
                                        OPTIONAL { entity:" + ids[i] + " wdt:P166 ?awardID. " +
                                     " ?awardID rdfs:label ?award filter(lang(?award) = 'en').  } }";
                        results = endpoint.QueryWithResultSet(query);
                        foreach (SparqlResult result in results)
                        {
                            award = "";
                            try { award = result["award"].ToString().Remove(result["award"].ToString().Length - 3).Trim(); }
                            catch { }
                            if (award != "")
                            {
                                richTextBox1.Text += award + "\n";
                                awardPrm.Value = award;
                                rd = db.ExecuteReader("select id from awards where lower(award)=lower(?award)");
                                if (rd.HasRows)
                                    while (rd.Read())
                                    {
                                        awards_ids += rd.GetInt32("id") + ",";
                                    }
                                else
                                {
                                    awards_ids += db.ExecuteNonQuery("insert into awards(award) values(?award)").ToString() + ",";
                                }
                            }
                        }
                        richTextBox1.Text += "\nAwards ids: " + awards_ids + "\n";

                        //get description from wikipedia

                        string[] words = wikipedia.Split('/');
                        string title = words[words.Length - 1].Trim();
                        query = @"SELECT ?description WHERE { 
                                    <http://dbpedia.org/resource/" + title + "> <http://dbpedia.org/ontology/abstract> ?description. " +
                                    " filter(langMatches(lang(?description), 'EN')) }";
                        //richTextBox1.Text += query + "\n";
                        results = endpoint2.QueryWithResultSet(query);

                        foreach (SparqlResult result in results)
                        {
                            description = "";
                            try
                            {
                                description = result["description"].ToString().Remove(result["description"].ToString().Length - 3).Trim();
                                richTextBox1.Text += "\nDescription: " + description + "\n\n";
                            }
                            catch { }
                        }

                        //download image
                        string img_name = "";
                        if (image != "")
                        {
                            string[] split = image.Split('/');
                            img_name = split[split.Length - 1].Replace("?width=300", "");
                            try
                            {
                                webClient.DownloadFile(new Uri(image), "actors/" + img_name);
                            }
                            catch { }

                        }

                        //insert in database

                        subtitlePrm.Value = subtitle;
                        if (gender == "female") genderPrm.Value = 2;
                        else if (gender == "male")
                            genderPrm.Value = 1;
                        dateBirthPrm.Value = date_of_birth;
                        dateDeathPrm.Value = date_of_death;
                        place_birthPrm.Value = place_birth;
                        university_idsPrm.Value = education_ids;
                        imdbPrm.Value = imdb;
                        wikipediaPrm.Value = wikipedia;
                        descriptionPrm.Value = description;
                        imgPrm.Value = img_name;
                        award_idsPrm.Value = awards_ids;
                        idPrm.Value = idPrm;
                        idPrm.Value = ids[i];
                        db.ExecuteNonQuery("update actors set description=?description,subtitle=?subtitle,gender=?gender,image=?img,date_of_birth=?date_of_birth,date_of_death=?date_of_death, " +
                       "place_birth=?place_birth,educated_id=?university_ids,imdb=?imdb,wikipedia=?wikipedia,awards_id=?awards_id where wikidata_id=?wikidata_id");


                    }
                }
                db.Close();
            }
            catch (Exception e)
            {
                richTextBox1.Text += e.ToString();
            }
        }

        public void getCreators(int option)
        {
            try
            {
                // option 1 get id and title from wikidata
                // option 2 get more info about creators from wikidata
                database db = new database(database.maindb);
                List<string> ids = new List<string>();
                Dictionary<string, string> ids_wikipedia = new Dictionary<string, string>();
                SparqlResultSet results;
                string creators_ids = "";
                string creator = "";
                string wiki_id = "";

                if (option == 1)
                {
                    MySqlDataReader rd = db.ExecuteReader("select wikidata_id from series");
                    while (rd.Read())
                    {
                        ids.Add(rd.GetString("wikidata_id"));
                    }
                    SparqlRemoteEndpoint endpoint = new SparqlRemoteEndpoint(new Uri("https://query.wikidata.org/sparql"), "https://query.wikidata.org");
                    MySqlParameter serie_id = db.Command.Parameters.Add("?serie_id", MySqlDbType.String);
                    MySqlParameter creator_id = db.Command.Parameters.Add("?creator_id", MySqlDbType.String);
                    MySqlParameter creatorPrm = db.Command.Parameters.Add("?creator", MySqlDbType.String);
                    MySqlParameter creator_idsPrm = db.Command.Parameters.Add("?creator_ids", MySqlDbType.String);

                    richTextBox1.Text += ids.Count + "\n";

                    for (int i = 15000; i < ids.Count; i++)
                    {
                        if (ids[i] == "Q660263")
                        {
                            richTextBox1.Text += "Here:" + i + "\n";
                            //   break; 
                        }
                        richTextBox1.Text += i + " , " + ids[i] + "\n";
                        creators_ids = "";
                        string query = @"PREFIX entity: <http://www.wikidata.org/entity/>
                                    PREFIX wdt: <http://www.wikidata.org/prop/direct/>
                                    SELECT ?creatorID ?creator WHERE {
                                    OPTIONAL { entity:" + ids[i] + " wdt:P170 ?creatorID." +
                                        " ?creatorID rdfs:label ?creator. filter(lang(?creator) = 'en').       } }";
                        //richTextBox1.Text += query;
                        results = endpoint.QueryWithResultSet(query);
                        foreach (SparqlResult result in results)
                        {
                            creator = "";
                            wiki_id = "";
                            try
                            {
                                creator = result["creator"].ToString();
                                creator = creator.Remove(creator.Length - 3).Trim();
                                wiki_id = result["creatorID"].ToString().Substring(result["creatorID"].ToString().IndexOf("Q")).Trim();

                            }
                            catch { }
                            if (creator != "")
                            {
                                richTextBox1.Text += creator + "\n" + wiki_id + "\n";
                                creatorPrm.Value = creator;
                                creator_id.Value = wiki_id;
                                rd = db.ExecuteReader("select id from creation where wikidata_id=?creator_id");
                                if (rd.HasRows)
                                    while (rd.Read())
                                    {
                                        creators_ids += rd.GetInt32("id") + ",";
                                    }
                                else
                                {
                                    //type 1 for creator, 2 for composer
                                    creators_ids += db.ExecuteNonQuery("insert into creation(wikidata_id,name,type) values(?creator_id,?creator,1)").ToString() + ",";
                                }
                            }

                        }
                        if (creators_ids != "")
                        {
                            richTextBox1.Text += creators_ids + "\n";
                            creator_idsPrm.Value = creators_ids;
                            serie_id.Value = ids[i];
                            db.ExecuteNonQuery("update series set creator_ids=?creator_ids where wikidata_id=?serie_id");
                        }
                    }
                }
                //get more info from wikidata about actors
                else if (option == 2)
                {
                    WebClient webClient = new WebClient();
                    string gender, image, date_of_birth, date_of_death, place_birth, subtitle, imdb, wikipedia, educated, education_ids, description;
                    MySqlDataReader rd = db.ExecuteReader("select wikidata_id from creation");
                    while (rd.Read())
                    {
                        ids.Add(rd.GetString("wikidata_id"));
                    }
                    SparqlRemoteEndpoint endpoint = new SparqlRemoteEndpoint(new Uri("https://query.wikidata.org/sparql"), "https://query.wikidata.org");
                    SparqlRemoteEndpoint endpoint2 = new SparqlRemoteEndpoint(new Uri("http://dbpedia.org/sparql"), "http://dbpedia.org");
                    MySqlParameter idPrm = db.Command.Parameters.Add("?wikidata_id", MySqlDbType.String);
                    MySqlParameter creator_id = db.Command.Parameters.Add("?actor_id", MySqlDbType.String);
                    MySqlParameter universityPrm = db.Command.Parameters.Add("?university", MySqlDbType.String);
                    MySqlParameter university_idsPrm = db.Command.Parameters.Add("?university_ids", MySqlDbType.String);
                    MySqlParameter descriptionPrm = db.Command.Parameters.Add("?description", MySqlDbType.String);
                    MySqlParameter genderPrm = db.Command.Parameters.Add("?gender", MySqlDbType.Int16);
                    MySqlParameter dateBirthPrm = db.Command.Parameters.Add("?date_of_birth", MySqlDbType.String);
                    MySqlParameter dateDeathPrm = db.Command.Parameters.Add("?date_of_death", MySqlDbType.String);
                    MySqlParameter place_birthPrm = db.Command.Parameters.Add("?place_birth", MySqlDbType.String);
                    MySqlParameter subtitlePrm = db.Command.Parameters.Add("?subtitle", MySqlDbType.String);
                    MySqlParameter imdbPrm = db.Command.Parameters.Add("?imdb", MySqlDbType.String);
                    MySqlParameter wikipediaPrm = db.Command.Parameters.Add("?wikipedia", MySqlDbType.String);
                    MySqlParameter imgPrm = db.Command.Parameters.Add("?img", MySqlDbType.String);



                    richTextBox1.Text += ids.Count;
                    for (int i = 2307; i < 2500; i++)
                    {
                        gender = ""; image = ""; date_of_birth = ""; date_of_death = ""; place_birth = ""; subtitle = ""; imdb = ""; wikipedia = ""; educated = ""; education_ids = "";
                        description = "";
                        richTextBox1.Text += i + " , " + ids[i] + "\n";
                        string query = @"PREFIX entity: <http://www.wikidata.org/entity/>
                                        PREFIX wdt: <http://www.wikidata.org/prop/direct/>
                                        PREFIX schema: <http://schema.org/>

                                        SELECT ?gender ?image ?date_of_birth ?date_of_death ?place_birth ?subtitle ?imdb ?article WHERE {
                                        OPTIONAL { entity:" + ids[i] + " schema:description ?subtitle filter (lang(?subtitle) = 'en'). } " +
                                        "OPTIONAL { entity:" + ids[i] + " wdt:P21 ?genderID. " +
                                       " ?genderID rdfs:label ?gender filter(lang(?gender) = 'en').  } " +
                                       " OPTIONAL { entity:" + ids[i] + " wdt:P18 ?image.  } " +
                                      "  OPTIONAL { entity:" + ids[i] + " wdt:P569 ?date_of_birth.  } " +
                                       " OPTIONAL { entity:" + ids[i] + " wdt:P570 ?date_of_death.  } " +
                                      "  OPTIONAL { entity:" + ids[i] + " wdt:P19 ?place_birthID. " +
                                      "  ?place_birthID rdfs:label ?place_birth filter(lang(?place_birth) = 'en').   } " +
                                      "  OPTIONAL { entity:" + ids[i] + " wdt:P345 ?imdb.  } " +
                                      "  OPTIONAL { ?article schema:about entity:" + ids[i] + " . ?article schema:inLanguage 'en' . FILTER (SUBSTR(str(?article), 1, 25) = 'https://en.wikipedia.org/'). } }";
                        // richTextBox1.Text += query+"\n\n";
                        results = endpoint.QueryWithResultSet(query);
                        foreach (SparqlResult result in results)
                        {
                            try { gender = result["gender"].ToString().Remove(result["gender"].ToString().Length - 3).Trim(); }
                            catch { }
                            try { image = result["image"].ToString().Trim().Replace("%20", "_").Replace("%28", "(").Replace("%29", ")"); }
                            catch { }
                            try { date_of_birth = result["date_of_birth"].ToString().Remove(10).Trim(); }
                            catch { }
                            try { date_of_death = result["date_of_death"].ToString().Remove(10).Trim(); }
                            catch { }
                            try { place_birth = result["place_birth"].ToString().Remove(result["place_birth"].ToString().Length - 3).Trim(); }
                            catch { }
                            try { subtitle = result["subtitle"].ToString().Remove(result["subtitle"].ToString().Length - 3).Trim(); }
                            catch { }
                            try { imdb = result["imdb"].ToString().Trim(); }
                            catch { }
                            try
                            {
                                wikipedia = result["article"].ToString().Trim();
                                wikipedia = wikipedia.Replace("%20", "_").Replace("%28", "(").Replace("%29", ")").Replace("%C3%89", "É").Replace("'", "%27")
                               .Replace("%3A", ":").Replace("%26", "&").Replace("%24", "$").Replace("%2A", "*").Replace("%21", "!").Replace("%2C", ",").Replace("%C3%AE", "î");
                            }
                            catch { }
                        }
                        richTextBox1.Text += "Subtitle: " + subtitle + "\nGender: " + gender + "\n" + "Image: " + image + "\nDate of birth: " + date_of_birth + "\nDate of death: " + date_of_death +
                            "\nPlace birth: " + place_birth + "\nImdb: " + imdb + "\nWikipedia: " + wikipedia + "\nEducated:";

                        //get College
                        query = @"PREFIX entity: <http://www.wikidata.org/entity/>
                                        PREFIX wdt: <http://www.wikidata.org/prop/direct/>
                                        SELECT ?educated  WHERE {
                                        OPTIONAL { entity:" + ids[i] + " wdt:P69 ?educatedID. " +
                                      " ?educatedID rdfs:label ?educated filter(lang(?educated) = 'en').  } }";
                        results = endpoint.QueryWithResultSet(query);
                        foreach (SparqlResult result in results)
                        {
                            educated = "";
                            try { educated = result["educated"].ToString().Remove(result["educated"].ToString().Length - 3).Trim(); }
                            catch { }
                            if (educated != "")
                            {
                                richTextBox1.Text += educated + "\n";
                                universityPrm.Value = educated;
                                rd = db.ExecuteReader("select id from education where lower(name)=lower(?university)");
                                if (rd.HasRows)
                                    while (rd.Read())
                                    {
                                        education_ids += rd.GetInt32("id") + ",";
                                    }
                                else
                                {
                                    education_ids += db.ExecuteNonQuery("insert into education(name) values(?university)").ToString() + ",";
                                }
                            }
                        }
                        richTextBox1.Text += "\nEducation ids: " + education_ids + "\n";

                        //get description from wikipedia
                        string[] words = wikipedia.Split('/');
                        string title = words[words.Length - 1].Trim();
                        query = @"SELECT ?description WHERE { 
                                    <http://dbpedia.org/resource/" + title + "> <http://dbpedia.org/ontology/abstract> ?description. " +
                                    " filter(langMatches(lang(?description), 'EN')) }";
                        //richTextBox1.Text += query + "\n";
                        results = endpoint2.QueryWithResultSet(query);
                        foreach (SparqlResult result in results)
                        {
                            description = "";
                            try
                            {
                                description = result["description"].ToString().Remove(result["description"].ToString().Length - 3).Trim();
                                richTextBox1.Text += "\nDescription: " + description + "\n\n";
                            }
                            catch { }
                        }

                        //download image
                        string img_name = "";
                        if (image != "")
                        {
                            image = image.Replace("%20", "_").Replace("%28", "(").Replace("%29", ")");
                            string[] split = image.Split('/');
                            img_name = split[split.Length - 1].Replace("?width=300", "").Replace("%20", "_").Replace("%28", "(").Replace("%29", ")");
                            try
                            {
                                webClient.DownloadFile(new Uri(image), "creators/" + img_name);
                            }
                            catch { }

                        }

                        //insert in database

                        subtitlePrm.Value = subtitle;
                        if (gender == "female") genderPrm.Value = 2;
                        else if (gender == "male")
                            genderPrm.Value = 1;
                        dateBirthPrm.Value = date_of_birth;
                        dateDeathPrm.Value = date_of_death;
                        place_birthPrm.Value = place_birth;
                        university_idsPrm.Value = education_ids;
                        imdbPrm.Value = imdb;
                        wikipediaPrm.Value = wikipedia;
                        descriptionPrm.Value = description;
                        imgPrm.Value = img_name;
                        idPrm.Value = idPrm;
                        idPrm.Value = ids[i];
                        db.ExecuteNonQuery("update creation set description=?description,subtitle=?subtitle,gender=?gender,image=?img,date_of_birth=?date_of_birth,date_of_death=?date_of_death, " +
                       "place_birth=?place_birth,educated_id=?university_ids,imdb=?imdb,wikipedia=?wikipedia where wikidata_id=?wikidata_id");


                    }
                }
                db.Close();
            }
            catch (Exception e)
            {
                richTextBox1.Text += e.ToString() + "\n";
            }
        }
        public void getComposers(int option)
        { 
        try
            {
                // option 1 get id and title from wikidata
                // option 2 get more info about composers from wikidata
                database db = new database(database.maindb);
                List<string> ids = new List<string>();
                Dictionary<string, string> ids_wikipedia = new Dictionary<string, string>();
                SparqlResultSet results;
                string composer_ids = "";
                string composer = "";
                string wiki_id = "";

                if (option == 1)
                {
                    MySqlDataReader rd = db.ExecuteReader("select wikidata_id from series");
                    while (rd.Read())
                    {
                        ids.Add(rd.GetString("wikidata_id"));
                    }
                    SparqlRemoteEndpoint endpoint = new SparqlRemoteEndpoint(new Uri("https://query.wikidata.org/sparql"), "https://query.wikidata.org");
                    MySqlParameter serie_id = db.Command.Parameters.Add("?serie_id", MySqlDbType.String);
                    MySqlParameter composer_id = db.Command.Parameters.Add("?composer_id", MySqlDbType.String);
                    MySqlParameter composerPrm = db.Command.Parameters.Add("?composer", MySqlDbType.String);
                    MySqlParameter composer_idsPrm = db.Command.Parameters.Add("?composer_ids", MySqlDbType.String);

                    richTextBox1.Text += ids.Count + "\n";

                    for (int i = 15000; i < ids.Count; i++)
                    {
                        richTextBox1.Text += i + " , " + ids[i] + "\n";
                        composer_ids = "";
                        string query = @"PREFIX entity: <http://www.wikidata.org/entity/>
                                    PREFIX wdt: <http://www.wikidata.org/prop/direct/>
                                    SELECT ?composerID ?composer WHERE {
                                    OPTIONAL { entity:" + ids[i] + " wdt:P86 ?composerID." +
                                        " ?composerID rdfs:label ?composer. filter(lang(?composer) = 'en').       } }";
                        //richTextBox1.Text += query;
                        results = endpoint.QueryWithResultSet(query);
                        foreach (SparqlResult result in results)
                        {
                            composer = "";
                            wiki_id = "";
                            try
                            {
                                composer = result["composer"].ToString();
                                composer = composer.Remove(composer.Length - 3).Trim();
                                wiki_id = result["composerID"].ToString().Substring(result["composerID"].ToString().IndexOf("Q")).Trim();

                            }
                            catch { }
                            if (composer != "")
                            {
                                richTextBox1.Text += composer + "\n" + wiki_id + "\n";
                                composerPrm.Value = composer;
                                composer_id.Value = wiki_id;
                                rd = db.ExecuteReader("select id from creation where wikidata_id=?composer_id and type=2");
                                if (rd.HasRows)
                                    while (rd.Read())
                                    {
                                        composer_ids += rd.GetInt32("id") + ",";
                                    }
                                else
                                {
                                    //type 1 for creator, 2 for composer
                                    composer_ids += db.ExecuteNonQuery("insert into creation(wikidata_id,name,type) values(?composer_id,?composer,2)").ToString() + ",";
                                }
                            }

                        }
                        if (composer_ids != "")
                        {
                            richTextBox1.Text += composer_ids + "\n";
                            composer_idsPrm.Value = composer_ids;
                            serie_id.Value = ids[i];
                            db.ExecuteNonQuery("update series set composer_id=?composer_ids where wikidata_id=?serie_id");
                        }
                    }
                }
                //get more info from wikidata about actors
                else if (option == 2)
                {
                    WebClient webClient = new WebClient();
                    string gender, image, date_of_birth, date_of_death, place_birth, subtitle, imdb, wikipedia, educated, education_ids, description;
                    MySqlDataReader rd = db.ExecuteReader("select wikidata_id from creation where type=2");
                    while (rd.Read())
                    {
                        ids.Add(rd.GetString("wikidata_id"));
                    }
                    SparqlRemoteEndpoint endpoint = new SparqlRemoteEndpoint(new Uri("https://query.wikidata.org/sparql"), "https://query.wikidata.org");
                    SparqlRemoteEndpoint endpoint2 = new SparqlRemoteEndpoint(new Uri("http://dbpedia.org/sparql"), "http://dbpedia.org");
                    MySqlParameter idPrm = db.Command.Parameters.Add("?wikidata_id", MySqlDbType.String);
                    MySqlParameter creator_id = db.Command.Parameters.Add("?actor_id", MySqlDbType.String);
                    MySqlParameter universityPrm = db.Command.Parameters.Add("?university", MySqlDbType.String);
                    MySqlParameter university_idsPrm = db.Command.Parameters.Add("?university_ids", MySqlDbType.String);
                    MySqlParameter descriptionPrm = db.Command.Parameters.Add("?description", MySqlDbType.String);
                    MySqlParameter genderPrm = db.Command.Parameters.Add("?gender", MySqlDbType.Int16);
                    MySqlParameter dateBirthPrm = db.Command.Parameters.Add("?date_of_birth", MySqlDbType.String);
                    MySqlParameter dateDeathPrm = db.Command.Parameters.Add("?date_of_death", MySqlDbType.String);
                    MySqlParameter place_birthPrm = db.Command.Parameters.Add("?place_birth", MySqlDbType.String);
                    MySqlParameter subtitlePrm = db.Command.Parameters.Add("?subtitle", MySqlDbType.String);
                    MySqlParameter imdbPrm = db.Command.Parameters.Add("?imdb", MySqlDbType.String);
                    MySqlParameter wikipediaPrm = db.Command.Parameters.Add("?wikipedia", MySqlDbType.String);
                    MySqlParameter imgPrm = db.Command.Parameters.Add("?img", MySqlDbType.String);



                    richTextBox1.Text += ids.Count;
                    for (int i = 500; i < ids.Count; i++)
                    {
                        gender = ""; image = ""; date_of_birth = ""; date_of_death = ""; place_birth = ""; subtitle = ""; imdb = ""; wikipedia = ""; educated = ""; education_ids = "";
                        description = "";
                        richTextBox1.Text += i + " , " + ids[i] + "\n";
                        string query = @"PREFIX entity: <http://www.wikidata.org/entity/>
                                        PREFIX wdt: <http://www.wikidata.org/prop/direct/>
                                        PREFIX schema: <http://schema.org/>

                                        SELECT ?gender ?image ?date_of_birth ?date_of_death ?place_birth ?subtitle ?imdb ?article WHERE {
                                        OPTIONAL { entity:" + ids[i] + " schema:description ?subtitle filter (lang(?subtitle) = 'en'). } " +
                                        "OPTIONAL { entity:" + ids[i] + " wdt:P21 ?genderID. " +
                                       " ?genderID rdfs:label ?gender filter(lang(?gender) = 'en').  } " +
                                       " OPTIONAL { entity:" + ids[i] + " wdt:P18 ?image.  } " +
                                      "  OPTIONAL { entity:" + ids[i] + " wdt:P569 ?date_of_birth.  } " +
                                       " OPTIONAL { entity:" + ids[i] + " wdt:P570 ?date_of_death.  } " +
                                      "  OPTIONAL { entity:" + ids[i] + " wdt:P19 ?place_birthID. " +
                                      "  ?place_birthID rdfs:label ?place_birth filter(lang(?place_birth) = 'en').   } " +
                                      "  OPTIONAL { entity:" + ids[i] + " wdt:P345 ?imdb.  } " +
                                      "  OPTIONAL { ?article schema:about entity:" + ids[i] + " . ?article schema:inLanguage 'en' . FILTER (SUBSTR(str(?article), 1, 25) = 'https://en.wikipedia.org/'). } }";
                        // richTextBox1.Text += query+"\n\n";
                        results = endpoint.QueryWithResultSet(query);
                        foreach (SparqlResult result in results)
                        {
                            try { gender = result["gender"].ToString().Remove(result["gender"].ToString().Length - 3).Trim(); }
                            catch { }
                            try { image = result["image"].ToString().Trim().Replace("%20", "_").Replace("%28", "(").Replace("%29", ")"); }
                            catch { }
                            try { date_of_birth = result["date_of_birth"].ToString().Remove(10).Trim(); }
                            catch { }
                            try { date_of_death = result["date_of_death"].ToString().Remove(10).Trim(); }
                            catch { }
                            try { place_birth = result["place_birth"].ToString().Remove(result["place_birth"].ToString().Length - 3).Trim(); }
                            catch { }
                            try { subtitle = result["subtitle"].ToString().Remove(result["subtitle"].ToString().Length - 3).Trim(); }
                            catch { }
                            try { imdb = result["imdb"].ToString().Trim(); }
                            catch { }
                            try
                            {
                                wikipedia = result["article"].ToString().Trim();
                                wikipedia = wikipedia.Replace("%20", "_").Replace("%28", "(").Replace("%29", ")").Replace("%C3%89", "É").Replace("'", "%27")
                                                               .Replace("%3A", ":").Replace("%26", "&").Replace("%24", "$").Replace("%2A", "*").Replace("%21", "!").Replace("%2C", ",").Replace("%C3%AE", "î");
                            }
                            catch { }
                        }
                        richTextBox1.Text += "Subtitle: " + subtitle + "\nGender: " + gender + "\n" + "Image: " + image + "\nDate of birth: " + date_of_birth + "\nDate of death: " + date_of_death +
                            "\nPlace birth: " + place_birth + "\nImdb: " + imdb + "\nWikipedia: " + wikipedia + "\nEducated:";

                        //get College
                        query = @"PREFIX entity: <http://www.wikidata.org/entity/>
                                        PREFIX wdt: <http://www.wikidata.org/prop/direct/>
                                        SELECT ?educated  WHERE {
                                        OPTIONAL { entity:" + ids[i] + " wdt:P69 ?educatedID. " +
                                      " ?educatedID rdfs:label ?educated filter(lang(?educated) = 'en').  } }";
                        results = endpoint.QueryWithResultSet(query);
                        foreach (SparqlResult result in results)
                        {
                            educated = "";
                            try { educated = result["educated"].ToString().Remove(result["educated"].ToString().Length - 3).Trim(); }
                            catch { }
                            if (educated != "")
                            {
                                richTextBox1.Text += educated + "\n";
                                universityPrm.Value = educated;
                                rd = db.ExecuteReader("select id from education where lower(name)=lower(?university)");
                                if (rd.HasRows)
                                    while (rd.Read())
                                    {
                                        education_ids += rd.GetInt32("id") + ",";
                                    }
                                else
                                {
                                    education_ids += db.ExecuteNonQuery("insert into education(name) values(?university)").ToString() + ",";
                                }
                            }
                        }
                        richTextBox1.Text += "\nEducation ids: " + education_ids + "\n";

                        //get description from wikipedia
                        string[] words = wikipedia.Split('/');
                        string title = words[words.Length - 1].Trim();
                        query = @"SELECT ?description WHERE { 
                                    <http://dbpedia.org/resource/" + title + "> <http://dbpedia.org/ontology/abstract> ?description. " +
                                                            " filter(langMatches(lang(?description), 'EN')) }";
                        //richTextBox1.Text += query + "\n";
                        results = endpoint2.QueryWithResultSet(query);
                        foreach (SparqlResult result in results)
                        {
                            description = "";
                            try
                            {
                                description = result["description"].ToString().Remove(result["description"].ToString().Length - 3).Trim();
                                richTextBox1.Text += "\nDescription: " + description + "\n\n";
                            }
                            catch { }
                        }

                        //download image
                        string img_name = "";
                        if (image != "")
                        {
                            image = image.Replace("%20", "_").Replace("%28", "(").Replace("%29", ")");
                            string[] split = image.Split('/');
                            img_name = split[split.Length - 1].Replace("?width=300", "").Replace("%20", "_").Replace("%28", "(").Replace("%29", ")");
                            try
                            {
                                webClient.DownloadFile(new Uri(image), "composers/" + img_name);
                            }
                            catch { }

                        }

                        //insert in database

                        subtitlePrm.Value = subtitle;
                        if (gender == "female") genderPrm.Value = 2;
                        else if (gender == "male")
                            genderPrm.Value = 1;
                        dateBirthPrm.Value = date_of_birth;
                        dateDeathPrm.Value = date_of_death;
                        place_birthPrm.Value = place_birth;
                        university_idsPrm.Value = education_ids;
                        imdbPrm.Value = imdb;
                        wikipediaPrm.Value = wikipedia;
                        descriptionPrm.Value = description;
                        imgPrm.Value = img_name;
                        idPrm.Value = idPrm;
                        idPrm.Value = ids[i];
                        db.ExecuteNonQuery("update creation set description=?description,subtitle=?subtitle,gender=?gender,image=?img,date_of_birth=?date_of_birth,date_of_death=?date_of_death, " +
                       "place_birth=?place_birth,educated_id=?university_ids,imdb=?imdb,wikipedia=?wikipedia where wikidata_id=?wikidata_id and type=2");


                    }
                }
                db.Close();
            }
            catch (Exception e)
            {
                richTextBox1.Text += e.ToString() + "\n";
            }
        }

        public void DeleteSeriesWithoutTitle()
        {
            try
            {
                List<string> ids = new List<string>();
                string title = "", imdb = "", wikipedia = "";
                database db = new database(database.maindb);
                MySqlDataReader rd = db.ExecuteReader("select wikidata_id from series");
                while (rd.Read())
                {
                    ids.Add(rd.GetString("wikidata_id"));
                }
                int count = 0, good_count = 0;
                richTextBox1.Text = richTextBox1.Text + ids.Count;
                SparqlRemoteEndpoint endpoint = new SparqlRemoteEndpoint(new Uri("https://query.wikidata.org/sparql"), "https://query.wikidata.org");
                SparqlResultSet results;
                MySqlParameter wikidata_id = db.Command.Parameters.Add("?wikidata_id", MySqlDbType.String);
                for (int i = 0; i < ids.Count; i++)
                {
                    title = ""; imdb = ""; wikipedia = "";

                    string query = @"PREFIX entity: <http://www.wikidata.org/entity/>
                                     PREFIX wdt: <http://www.wikidata.org/prop/direct/>
                                     PREFIX schema: <http://schema.org/>

                                                            SELECT ?title ?imdb ?article WHERE {
                         OPTIONAL { entity:" + ids[i] + " rdfs:label ?title filter(lang(?title) = 'en'). } OPTIONAL { entity:" + ids[i] + " wdt:P345 ?imdb. }" +
                        "OPTIONAL { ?article schema:about entity:" + ids[i] + " . ?article schema:inLanguage 'en' . FILTER (SUBSTR(str(?article), 1, 25) = 'https://en.wikipedia.org/'). } }";
                    // richTextBox1.Text += query + "\n";
                    results = endpoint.QueryWithResultSet(query);
                    foreach (SparqlResult result in results)
                    {

                        try
                        { imdb = result["imdb"].ToString(); }
                        catch { }
                        try
                        { title = result["title"].ToString(); }
                        catch { }
                        try
                        { wikipedia = result["article"].ToString(); }
                        catch { }
                    }
                    if (title == "" || wikipedia == "")
                    {
                        count++;
                        richTextBox1.Text += "Rejected:" + ids[i] + "\n";
                        wikidata_id.Value = ids[i];
                        db.ExecuteNonQuery("delete from series where wikidata_id=?wikidata_id");
                    }
                    else
                    {
                        good_count++;
                        richTextBox1.Text += "OK::" + ids[i] + "\n";
                    }
                }
                richTextBox1.Text += "Count:" + count + "\nGood count:" + good_count;
                db.Close();
            }
            catch (Exception e)
            {
                richTextBox1.Text += e.ToString();
            }
        }
        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Cursor = Cursors.Default;
            richTextBox1.Text = richTextBox1.Text + "Finished";
        }

        public void getIMDBImg()
        {
            try
            {
                database db = new database(database.maindb);
                Dictionary<int, Series> series = new Dictionary<int, Series>();
                MySqlDataReader rd = db.ExecuteReader("select id,img,imdb from series where imdb!='' and id<5277");
                while(rd.Read())
                {
                    series.Add(rd.GetInt32("id"), new Series(rd.GetString("imdb"), rd.GetString("img")));
                }

                int count = 0;
                MySqlParameter id = db.Command.Parameters.Add("?id", MySqlDbType.Int32);
                MySqlParameter img = db.Command.Parameters.Add("?img", MySqlDbType.String);
                foreach (KeyValuePair<int, Series> serie in series)
                {
                    
                    count++;
                    if(count>0&& count<2000)
                    using (WebClient wc = new WebClient())
                    {
                            richTextBox1.Text += "Count:\n" + count + "Id: " + serie.Key + "\nImdb: " + serie.Value.imdb + "\nImg: " + serie.Value.image + "\n";
                            string image_url = "";
                        var json = wc.DownloadString("http://www.omdbapi.com/?i="+serie.Value.imdb+"&plot=short&r=json");
                        Dictionary<string, string> values = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
                        try
                        {
                            image_url = values["Poster"];
                            int pos = image_url.IndexOf("_V1");
                            richTextBox1.Text += pos + "\n";
                            image_url = image_url.Remove(pos) + ".jpg";
                           
                            richTextBox1.Text += "Image URL: " + image_url + "\n";
                            string[] split = image_url.Split('/');
                            string image = split[split.Length - 1];
                            richTextBox1.Text += "Image: " + image + "\n\n";

                                if (!String.IsNullOrEmpty(image_url))
                                {
                                    try
                                    {
                                        System.IO.File.Delete("images/" + serie.Value.image);
                                        richTextBox1.Text += "Deleting file...\n\n";
                                    }
                                    catch { richTextBox1.Text += "THIS FILE DOESNT EXIST\n\n"; }

                                    wc.DownloadFileAsync(new Uri(image_url), "series/" + image);

                                    id.Value = serie.Key;
                                    img.Value = image;
                                    db.ExecuteNonQuery("update series set img=?img where id=?id");
                                }
                                else richTextBox1.Text += "Imdb doesnt have image, but the serie has";
                        }
                        catch (Exception e){ richTextBox1.Text += e.ToString();  }
                    }
                   // if (count == 10) break;
                }
            }
            catch
            { }
        }

        public void getNextEpisode()
        {
            try
            {
                database db = new database(database.maindb);
                MySqlDataReader rd = db.ExecuteReader("select id,imdb from series where imdb!=''");
                int count = 0;
                string next_episode = "";
                HttpWebRequest request;
                Dictionary<int, NextEpisode> episodes = new Dictionary<int, NextEpisode>();

                while (rd.Read())
                {
                    count++;
                    int id = rd.GetInt32("id");
                    string imdb = rd.GetString("Imdb");
                    next_episode = "";
                  //  if (rd.GetInt32("id") == 52)
                    {
                        richTextBox1.Text += "Id:" + rd.GetInt32("id") + "\nImdb:" + rd.GetString("Imdb") + "\n";
                        try
                        {
                            request = (HttpWebRequest)WebRequest.Create("http://api.tvmaze.com/lookup/shows?imdb=" + imdb);
                            request.AllowAutoRedirect = false;
                            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                            string redirUrl = response.Headers["Location"];
                            string content = "";
                            using (WebClient wc = new WebClient())
                            {
                                content = "["+wc.DownloadString(redirUrl)+"]";
                            }
                            response.Close();
                           // richTextBox1.Text += content + "\n\n";

                            JArray jsonInfo = JArray.Parse(content);
                           // richTextBox1.Text += jsonInfo + "\n\n";


                            try
                            {
                               // richTextBox1.Text += jsonInfo[0]["_links"].ToString() + "\n";
                                next_episode = jsonInfo[0]["_links"]["nextepisode"]["href"].ToString()+"\n";
                            }
                            catch (Exception e) { //richTextBox1.Text += e.ToString(); 
                            }
                        }
                        catch (Exception e) {// richTextBox1.Text += e.ToString(); 
                        }
                        if (next_episode != "") {
                            richTextBox1.Text += next_episode;
                            request = (HttpWebRequest)WebRequest.Create(next_episode);
                            request.AllowAutoRedirect = false;
                            Stream objStream =request.GetResponse().GetResponseStream();
                            StreamReader objReader = new StreamReader(objStream);
                            string content = "["+objReader.ReadToEnd()+"]";
                            JArray jsonInfo = JArray.Parse(content);
                            string nr = jsonInfo[0]["season"].ToString() + "x" + jsonInfo[0]["number"].ToString();
                            string airdate = jsonInfo[0]["airdate"].ToString();
                            episodes.Add(id, new NextEpisode(nr, airdate));
                            richTextBox1.Text += nr + " "+airdate+"\n";
                        }
                       
                    }
                    //if (count == 50) break;
                } 
                
                foreach(KeyValuePair<int,NextEpisode> episode in episodes)
                {
                    db.ExecuteNonQuery("update series set next_episode_airdate='" + episode.Value.airdate + "',next_episode_number='" + episode.Value.nr + "' where id=" + episode.Key);
                }

                db.Close();
            }
            catch(Exception e)
            {
                richTextBox1.Text += e.ToString();
            }
        }
    }
}
