﻿using System;

public class Series
{
    public string imdb;
    public string image;
	public Series(string imdb,string image)
	{
        this.image = image;
        this.imdb = imdb;
	}
}

public class NextEpisode
{
    public string nr;
    public string airdate;

    public NextEpisode(string nr,string airdate)
    {
        this.nr = nr;
        this.airdate = airdate;
    }
}
