﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using TVSeries.Models;
using MySql.Data.MySqlClient;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace TVSeries.Controllers
{
    public class CategoriesController : Controller
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }
        static string UppercaseFirst(string s)
        {
            // Check for empty string.
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            // Return char and concat substring.
            return char.ToUpper(s[0]) + s.Substring(1);
        }

        public IActionResult genres()
        {
            try
            {
                database db = new database(database.maindb);
                Dictionary<int, string> genres = new Dictionary<int, string>();
                MySqlDataReader rd = db.ExecuteReader("select * from genres order by genre");
                while (rd.Read())
                    genres.Add(rd.GetInt32("id"), UppercaseFirst(rd.GetString("genre")));
                ViewBag.genres = genres;
                db.Close();

            }
            catch { }
            return View();
        }

        public IActionResult countries()
        {
            try
            {
                database db = new database(database.maindb);
                Dictionary<int, string> countries = new Dictionary<int, string>();
                MySqlDataReader rd = db.ExecuteReader("select * from countries order by country");
                while (rd.Read())
                    countries.Add(rd.GetInt32("id"), UppercaseFirst(rd.GetString("country")));
                ViewBag.countries = countries;
                db.Close();

            }
            catch { }
            return View();
        }

        public IActionResult networks()
        {
            try
            {
                database db = new database(database.maindb);
                Dictionary<int, string> networks = new Dictionary<int, string>();
                MySqlDataReader rd = db.ExecuteReader("select * from network order by network");
                while (rd.Read())
                    networks.Add(rd.GetInt32("id"), UppercaseFirst(rd.GetString("network")));
                ViewBag.networks = networks;
                db.Close();

            }
            catch { }
            return View();
        }

        public IActionResult languages()
        {
            try
            {
                database db = new database(database.maindb);
                Dictionary<int, string> languages = new Dictionary<int, string>();
                MySqlDataReader rd = db.ExecuteReader("select * from languages order by language");
                while (rd.Read())
                    languages.Add(rd.GetInt32("id"), UppercaseFirst(rd.GetString("language")));
                ViewBag.languages = languages;
                db.Close();

            }
            catch { }
            return View();
        }

        public IActionResult educations()
        {
            try
            {
                database db = new database(database.maindb);
                Dictionary<int, string> education = new Dictionary<int, string>();
                MySqlDataReader rd = db.ExecuteReader("select * from education order by name");
                while (rd.Read())
                    education.Add(rd.GetInt32("id"), UppercaseFirst(rd.GetString("name")));
                ViewBag.education = education;
                db.Close();

            }
            catch { }
            return View();
        }
    }
}
