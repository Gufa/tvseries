﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using TVSeries.Models;
using Microsoft.AspNet.Http;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System.Net;
using Newtonsoft.Json.Linq;
using Microsoft.Framework.Runtime;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace TVSeries.Controllers
{
    public class SeriesController : Controller
    {

        IApplicationEnvironment _hostingEnvironment;


        public SeriesController(IApplicationEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult title(int id)
        {
            try
            {
                ViewBag.id = id;
                ViewBag.path = "http://craigslist.gplay.ro/series/";
                string country = "";
                string network = "";
                Dictionary<int, string> languages = new Dictionary<int, string>();
                Dictionary<int, string> genres = new Dictionary<int, string>();
                Dictionary<int, string> actors = new Dictionary<int, string>();
                Dictionary<int, string> creators = new Dictionary<int, string>();
                Dictionary<int, string> composers = new Dictionary<int, string>();
                Dictionary<int, similar> similarSeries = new Dictionary<int, Models.similar>();
                List<Comment> comments = new List<Comment>();
                string language_id = "", genre_id = "", actor_ids = "";
                int country_id = 0, network_id = 0;
                string creator_ids = "";
                string description = "";
                string composer_ids = "";
                string title = "";
                string total_rating = "";
                database db = new database(database.maindb);
                db.AddParam("?id", id);
                MySqlDataReader rd = db.ExecuteReader("select * from series where id=?id");

                while (rd.Read())
                {
                    title = rd.GetString("title");
                    description = rd.GetString("description");

                    ViewBag.wikipedia = rd.GetString("wikipedia");
                    ViewBag.imdb = rd.GetString("imdb");
                    ViewBag.nr_episodes = rd.GetString("number_episodes");
                    ViewBag.nr_seasons = rd.GetString("number_seasons");
                    ViewBag.start_date = rd.GetString("start_date");
                    ViewBag.img = rd.GetString("img");
                    country_id = rd.GetInt16("country_id");
                    language_id = rd.GetString("language_id");
                    network_id = rd.GetInt16("network_id");
                    genre_id = rd.GetString("genre_id");
                    actor_ids = rd.GetString("actor_ids");
                    creator_ids = rd.GetString("creator_ids");
                    composer_ids = rd.GetString("composer_id");
                    if (rd.GetInt32("nr_rating") != 0)
                    {
                        total_rating = ((double)rd.GetInt32("rating") / rd.GetInt32("nr_rating")).ToString();
                        if (total_rating.Count() > 3) total_rating = total_rating.Remove(total_rating.IndexOf('.') + 2);
                        ViewBag.total_rating = total_rating;
                    }
                    else
                        ViewBag.total_rating = 0;


                }
                ViewBag.title = title;
                ViewBag.description = description;
                ViewBag.my_rating = 0;
                if (Context.Session.GetInt32("on") == 1)
                {
                    db.AddParam("?id_user", Context.Session.GetInt32("id"));
                    rd = db.ExecuteReader("select rating from rating where id_user=?id_user and id_serie=?id");
                    while (rd.Read())
                        ViewBag.my_rating = rd.GetInt32("rating");

                }

                db.AddParam("?country_id", country_id);
                rd = db.ExecuteReader("select * from countries where id=?country_id");
                while (rd.Read())
                {
                    country = rd.GetString("country");
                }
                ViewBag.country = country;
                ViewBag.country_id = country_id;

                db.AddParam("?network_id", network_id);
                rd = db.ExecuteReader("select * from network where id=?network_id");
                while (rd.Read())
                {
                    network = rd.GetString("network");
                }
                ViewBag.network = network;
                ViewBag.network_id = network_id;

                //get language
                if (language_id.Contains(","))
                {
                    string[] split = language_id.Split(',');
                    MySqlParameter langPrm = db.Command.Parameters.Add("?lang", MySqlDbType.Int32);
                    for (int i = 0; i < split.Length - 1; i++)
                    {
                        langPrm.Value = Convert.ToInt32(split[i]);
                        rd = db.ExecuteReader("select language from languages where id=?lang");
                        while (rd.Read())
                        {
                            languages.Add(Convert.ToInt32(split[i]), rd.GetString("language"));
                        }
                    }
                }
                else if (language_id != "")
                {
                    db.AddParam("?lang", Convert.ToInt32(language_id));
                    rd = db.ExecuteReader("select language from languages where id=?lang");
                    while (rd.Read())
                        languages.Add(Convert.ToInt32(language_id), rd.GetString("language"));
                }
                ViewBag.languages = languages;
                ViewBag.nrLanguages = languages.Count;

                //get actors
                if (actor_ids.Contains(","))
                {
                    actor_ids = actor_ids.Remove(0, 1);
                    string[] split = actor_ids.Split(',');
                    MySqlParameter actorPrm = db.Command.Parameters.Add("?actor", MySqlDbType.Int32);
                    for (int i = 0; i < split.Length - 1; i++)
                    {
                        actorPrm.Value = Convert.ToInt32(split[i]);
                        rd = db.ExecuteReader("select name from actors where id=?actor");
                        while (rd.Read())
                        {
                            actors.Add(Convert.ToInt32(split[i]), rd.GetString("name"));
                        }
                    }
                }
                else if (actor_ids != "")
                {
                    db.AddParam("?actor", actor_ids);
                    rd = db.ExecuteReader("select name from actors where id=?actor");
                    while (rd.Read())
                        actors.Add(Convert.ToInt32(actor_ids), rd.GetString("name"));
                }
                ViewBag.actors = actors;
                ViewBag.nrActors = actors.Count;

                //get genre
                if (genre_id.Contains(","))
                {
                    genre_id = genre_id.Remove(0, 1);
                    string[] split = genre_id.Split(',');
                    MySqlParameter genrePrm = db.Command.Parameters.Add("?genre", MySqlDbType.Int32);
                    for (int i = 0; i < split.Length - 1; i++)
                    {
                        genrePrm.Value = Convert.ToInt32(split[i]);
                        rd = db.ExecuteReader("select genre from genres where id=?genre");
                        while (rd.Read())
                        {
                            genres.Add(Convert.ToInt32(split[i]), rd.GetString("genre"));
                        }
                    }
                }
                else if (genre_id != "")
                {
                    db.AddParam("?genre", Convert.ToInt32(genre_id));
                    rd = db.ExecuteReader("select genre from genres where id=?genre");
                    while (rd.Read())
                        genres.Add(Convert.ToInt32(genre_id), rd.GetString("genre"));
                }
                ViewBag.genres = genres;
                ViewBag.nrgenres = genres.Count;

                //get creators
                if (creator_ids.Contains(","))
                {
                    creator_ids = creator_ids.Remove(0, 1);
                    string[] split = creator_ids.Split(',');
                    MySqlParameter creatorPrm = db.Command.Parameters.Add("?creator", MySqlDbType.Int32);
                    for (int i = 0; i < split.Length - 1; i++)
                    {
                        creatorPrm.Value = Convert.ToInt32(split[i]);
                        rd = db.ExecuteReader("select name from creation where id=?creator and type=1");
                        while (rd.Read())
                        {
                            creators.Add(Convert.ToInt32(split[i]), rd.GetString("name"));
                        }
                    }
                }
                else if (creator_ids != "")
                {
                    db.AddParam("?creator", Convert.ToInt32(creator_ids));
                    rd = db.ExecuteReader("select creator from creation where id=?creator and type=1");
                    while (rd.Read())
                        creators.Add(Convert.ToInt32(creator_ids), rd.GetString("creator"));
                }
                ViewBag.creators = creators;
                ViewBag.nrCreators = creators.Count;

                //get composers
                if (composer_ids.Contains(","))
                {
                    composer_ids = composer_ids.Remove(0, 1);
                    string[] split = composer_ids.Split(',');
                    MySqlParameter composerPrm = db.Command.Parameters.Add("?composer", MySqlDbType.Int32);
                    for (int i = 0; i < split.Length - 1; i++)
                    {
                        composerPrm.Value = Convert.ToInt32(split[i]);
                        rd = db.ExecuteReader("select name from creation where id=?composer and type=2");
                        while (rd.Read())
                        {
                            composers.Add(Convert.ToInt32(split[i]), rd.GetString("name"));
                        }
                    }
                }
                else if (composer_ids != "")
                {
                    db.AddParam("?composer", Convert.ToInt32(composer_ids));
                    rd = db.ExecuteReader("select composer from creation where id=?composer and type=2");
                    while (rd.Read())
                        composers.Add(Convert.ToInt32(composer_ids), rd.GetString("composer"));
                }
                ViewBag.composers = composers;
                ViewBag.nrComposers = composers.Count;


                //luam 3 seriale care au acelasi gen daca exista
                if (genres.Count > 0)
                {

                    string query = "select id,title,img from series where (genre_id like '%";
                    foreach (KeyValuePair<int, string> genre in genres)
                    {
                        if (genre.Value != genres.Values.Last()) query += genre.Key + ",%' or genre_id like '%  ";
                        else query += genre.Key + ",%') and id!=?id limit 3";
                    }
                    ViewBag.query = query;
                    rd = db.ExecuteReader(query);
                    while (rd.Read())
                    {
                        similarSeries.Add(rd.GetInt32("id"), new similar(rd.GetString("img"), rd.GetString("title")));
                    }


                }
                //in caz de by am gasit 3 seriale, luam alte 3 seriale random sau le completam pe cele gasite deja
                if (similarSeries.Count < 3)
                {
                    string query = "SELECT r1.id,title,img FROM series AS r1 JOIN (SELECT CEIL(RAND() * (SELECT MAX(id) FROM series)) AS id) AS r2 " +
                            "WHERE r1.id >= r2.id and r1.img != '' ORDER BY r1.id ASC LIMIT " + (3 - similarSeries.Count);
                    rd = db.ExecuteReader(query);
                    while (rd.Read())
                    {
                        similarSeries.Add(rd.GetInt32("id"), new similar(rd.GetString("img"), rd.GetString("title")));
                    }
                }
                ViewBag.similarSeries = similarSeries;
                ViewBag.nrSimilar = similarSeries.Count;

                rd = db.ExecuteReader("SELECT a.comment as comment,a.date as date,concat(b.firstname,' ',b.lastname) as name,b.id as id FROM `comments` a,users b where a.id_user=b.id and a.id_serie=?id order by a.date desc");
                while (rd.Read())
                {
                    string image_path = _hostingEnvironment.ApplicationBasePath + "\\wwwroot\\images\\users\\" + rd.GetInt32("id") + ".png";
                    string img = "";
                    if (System.IO.File.Exists(image_path))
                        img = "\\images\\users\\" + rd.GetInt32("id") + ".png";
                    comments.Add(new Comment(rd.GetString("comment"), rd.GetDateTime("date").ToString("yyyy-MM-dd HH:mm:ss"), rd.GetString("name"),img));
                }
                ViewBag.nrComments = comments.Count();
                ViewBag.comments = comments;


                //1 daca nu e adaugat, 2 daca e adaugat
                ViewBag.favorites = "Remove from favorites";
                ViewBag.myimg = "";
                if (Context.Session.GetInt32("on") == 1)
                {

                    string image_path = _hostingEnvironment.ApplicationBasePath + "\\wwwroot\\images\\users\\" + Context.Session.GetInt32("id") + ".png";
                    string img = "";
                    if (System.IO.File.Exists(image_path))
                        img = "..\\\\images\\\\users\\\\" + Context.Session.GetInt32("id") + ".png";
                    ViewBag.myimg = img;

                    // db.AddParam("?id_user", Context.Session.GetInt32("id"));
                    rd = db.ExecuteReader("select * from favorites where id_serie=?id and id_user=?id_user");
                    while (rd.Read())
                        if (rd.GetInt32("option") == 1) ViewBag.favorites = "Add to favorites";
                    if (!rd.HasRows) ViewBag.favorites = "Add to favorites";
                }
                else ViewBag.favorites = "Add to favorites";

                ViewBag.nrSeasons = 0;
                List<Episode> episodes = new List<Episode>();
                ViewBag.episodes = episodes;
                //get episodes and seasons from tvmaze api if imdb isn't null
                if (ViewBag.imdb != "")
                {
                    try
                    {
                        HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://api.tvmaze.com/lookup/shows?imdb=" + ViewBag.imdb);
                        request.AllowAutoRedirect = false;
                        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                        string redirUrl = response.Headers["Location"] + "/episodes";
                        string content = "";
                        using (WebClient wc = new WebClient())
                        {
                            content = wc.DownloadString(redirUrl);
                        }
                        response.Close();

                        JArray jsonInfo = JArray.Parse(content);
                        string last_season = jsonInfo[jsonInfo.Count - 1]["season"].ToString();

                        var lastSeasonEpisodes = jsonInfo.Where(x => x["season"].ToString() == last_season).ToList();
                        ViewBag.nrSeasons = last_season;
                        db.AddParam("?season", last_season);
                        int watched = 0;

                        string img_episode = "";
                        for (int i = 0; i < lastSeasonEpisodes.Count; i++)
                        {
                            if (Context.Session.GetInt32("on") == 1)
                            {
                                int nr_episode = Convert.ToInt32(lastSeasonEpisodes[i]["number"]);
                                rd = db.ExecuteReader("select * from watched where season=?season and id_user=?id_user and id_serie=?id and episodes like '%," + nr_episode + "%'");
                                if (rd.HasRows)
                                    watched = 1;
                                else watched = 0;
                            }
                            try
                            {
                                img_episode = lastSeasonEpisodes[i]["image"]["medium"].ToString();
                            }
                            catch { }
                            if (img_episode == "") img_episode = "../images/series/episode_noimage.png";
                            episodes.Add(new Episode(lastSeasonEpisodes[i]["name"].ToString(), Convert.ToInt32(lastSeasonEpisodes[i]["number"]), lastSeasonEpisodes[i]["airdate"].ToString(),
                                img_episode, lastSeasonEpisodes[i]["summary"].ToString(), watched));
                        }
                        ViewBag.episodes = episodes;
                    }
                    catch { }
                }

                //Show the redirected url

                db.Close();
            }
            catch (Exception e)
            {
                ViewBag.error = e.ToString();
            }
            return View();
        }
        [HttpPost]
        public string searchResult(string keywords)
        {
            try
            {
                database db = new database(database.maindb);
                db.AddParam("?keyword", keywords);
                Dictionary<int, string> series = new Dictionary<int, string>();
                MySqlDataReader rd = db.ExecuteReader("select id,title from series where title like '%" + keywords + "%' limit 5");
                while (rd.Read())
                    series.Add(rd.GetInt32("id"), rd.GetString("title"));
                db.Close();
                return JsonConvert.SerializeObject(series);
            }
            catch (Exception e)
            {
                return e.ToString();
            }

        }
        public IActionResult genre(int id)
        {
            try
            {
                ViewBag.path = "http://craigslist.gplay.ro/series/";
                Dictionary<int, Series> series = new Dictionary<int, Series>();
                database db = new database(database.maindb);
                db.AddParam("?id", id);
                string description = "";

                Dictionary<int, string> genres = new Dictionary<int, string>();
                MySqlDataReader rd = db.ExecuteReader("select nr_rating,rating/nr_rating as total_rating,id,title,description,img,genre_id from series where genre_id like '%," + id + ",%' limit 10");
                while (rd.Read())
                {
                    description = rd.GetString("description");
                    if (description.Length > 400)
                    {

                        description = description.Remove(400) + " ...";
                    }
                    string total_rating = "0";
                    if (rd.GetInt32("nr_rating") == 0)
                    { total_rating = "0"; }
                    else
                    {
                        total_rating = rd.GetDouble("total_rating").ToString();
                        if (total_rating.Count() > 3) total_rating = total_rating.Remove(total_rating.IndexOf('.') + 2);
                    }
                    series.Add(rd.GetInt32("id"), new Series(rd.GetString("title"), description, rd.GetString("img"), genres, rd.GetString("genre_id"), 0, "", total_rating));
                }

                int my_rating = 0;

                List<int> keys = new List<int>(series.Keys);
                if (Context.Session.GetInt32("on") == 1)
                {
                    db.AddParam("?id_user", Context.Session.GetInt32("id"));
                    MySqlParameter id_seriePrm = db.Command.Parameters.Add("?id_serie", MySqlDbType.Int32);
                    foreach (int key in keys)
                    {
                        id_seriePrm.Value = key;

                        try
                        {
                            rd = db.ExecuteReader("select rating from rating where id_user=?id_user and id_serie=?id_serie");
                            if (rd.HasRows)
                                while (rd.Read())
                                {
                                    my_rating = rd.GetInt32("rating");
                                    // serie.Value.my_rating[rd.GetInt32("rating")] = "checked";
                                    break;
                                }
                            else my_rating = 0;
                            series[key] = new Series(series[key].title, series[key].description, series[key].img, series[key].genres, series[key].genre_id, my_rating, "", series[key].total_rating);
                        }
                        catch (Exception e)
                        {
                            ViewBag.error = e.ToString();
                        }

                    }
                }


                rd = db.ExecuteReader("select count(*) as nr from series where genre_id like '%," + id + ",%' ");
                ViewBag.totalpages = 0;
                ViewBag.id = id;
                while (rd.Read())
                {
                    ViewBag.totalpages = rd.GetInt32("nr") / 10;
                }

                rd = db.ExecuteReader("select genre from genres where id=?id");
                while (rd.Read())
                {
                    ViewBag.title = UppercaseFirst(rd.GetString("genre"));
                }


                ViewBag.series = series;




                db.Close();
            }
            catch (Exception e)
            {
                ViewBag.error = e.ToString();
            }
            return View();
        }


        public IActionResult country(int id)
        {
            try
            {
                ViewBag.path = "http://craigslist.gplay.ro/series/";
                Dictionary<int, Series> series = new Dictionary<int, Series>();
                database db = new database(database.maindb);
                db.AddParam("?id", id);
                string description = "";
                string favorites = "Add to favorites";

                Dictionary<int, string> genres = new Dictionary<int, string>();
                MySqlDataReader rd = db.ExecuteReader("select nr_rating,rating/nr_rating as total_rating,id,title,description,img,genre_id from series where country_id=?id limit 10");
                while (rd.Read())
                {
                    description = rd.GetString("description");
                    if (description.Length > 400)
                    {
                        description = description.Remove(400) + " ...";
                    }
                    string total_rating = "0";
                    if (rd.GetInt32("nr_rating") == 0)
                    { total_rating = "0"; }
                    else
                    {
                        total_rating = rd.GetDouble("total_rating").ToString();
                        if (total_rating.Count() > 3) total_rating = total_rating.Remove(total_rating.IndexOf('.') + 2);
                    }
                    series.Add(rd.GetInt32("id"), new Series(rd.GetString("title"), description, rd.GetString("img"), genres, rd.GetString("genre_id"), 0, favorites, total_rating));
                }


                int my_rating = 0;

                List<int> keys = new List<int>(series.Keys);
                if (Context.Session.GetInt32("on") == 1)
                {
                    db.AddParam("?id_user", Context.Session.GetInt32("id"));
                    MySqlParameter id_seriePrm = db.Command.Parameters.Add("?id_serie", MySqlDbType.Int32);
                    foreach (int key in keys)
                    {
                        id_seriePrm.Value = key;

                        try
                        {
                            rd = db.ExecuteReader("select rating from rating where id_user=?id_user and id_serie=?id_serie");
                            if (rd.HasRows)
                                while (rd.Read())
                                {
                                    my_rating = rd.GetInt32("rating");
                                    // serie.Value.my_rating[rd.GetInt32("rating")] = "checked";
                                    break;
                                }
                            else my_rating = 0;
                            rd = db.ExecuteReader("select `option` from favorites where id_user=?id_user and id_serie=" + key);
                            if (rd.HasRows)
                                while (rd.Read())
                                    if (rd.GetInt32("option") == 2)
                                        favorites = "Remove from favorites";
                                    else favorites = "Add to favorites";
                            else favorites = "Add to favorites";
                            series[key] = new Series(series[key].title, series[key].description, series[key].img, series[key].genres, series[key].genre_id, my_rating, favorites, series[key].total_rating);
                        }
                        catch (Exception e)
                        {
                            ViewBag.error = e.ToString();
                        }

                    }
                }


                rd = db.ExecuteReader("select count(*) as nr from series where country_id=?id");
                ViewBag.totalpages = 0;
                ViewBag.id = id;
                while (rd.Read())
                {
                    ViewBag.totalpages = rd.GetInt32("nr") / 10;
                }

                rd = db.ExecuteReader("select country from countries where id=?id");
                while (rd.Read())
                {
                    ViewBag.title = UppercaseFirst(rd.GetString("country"));
                }


                ViewBag.series = series;




                db.Close();
            }
            catch (Exception e)
            {
                ViewBag.error = e.ToString();
            }
            return View();
        }

        public IActionResult creation(int id)
        {
            try
            {
                ViewBag.path = "http://craigslist.gplay.ro/creators";

                database db = new database(database.maindb);
                db.AddParam("?id", id);
                MySqlDataReader rd = db.ExecuteReader("select * from creation where id=?id");

                string educated_ids = "";
                Dictionary<int, string> education = new Dictionary<int, string>();

                while (rd.Read())
                {
                    ViewBag.name = rd.GetString("name");
                    ViewBag.description = rd.GetString("description");
                    ViewBag.subtitle = rd.GetString("subtitle");
                    ViewBag.wikipedia = rd.GetString("wikipedia");
                    ViewBag.genre = rd.GetInt32("id");
                    ViewBag.img = rd.GetString("image");
                    ViewBag.date_of_birth = rd.GetString("date_of_birth");
                    ViewBag.date_of_death = rd.GetString("date_of_death");
                    ViewBag.place_birth = rd.GetString("place_birth");
                    ViewBag.imdb = rd.GetString("imdb");
                    educated_ids = rd.GetString("educated_id");
                }


                if (educated_ids.Contains(","))
                {
                    educated_ids = educated_ids.Remove(0, 1);
                    string[] split = educated_ids.Split(',');
                    MySqlParameter educatedPrm = db.Command.Parameters.Add("?educated", MySqlDbType.Int32);
                    for (int i = 0; i < split.Length - 1; i++)
                    {
                        educatedPrm.Value = Convert.ToInt32(split[i]);
                        rd = db.ExecuteReader("select name from education where id=?educated");
                        while (rd.Read())
                        {
                            education.Add(Convert.ToInt32(split[i]), rd.GetString("name"));
                        }
                    }
                }
                else if (educated_ids != "")
                {
                    db.AddParam("?educated", Convert.ToInt32(educated_ids));
                    rd = db.ExecuteReader("select name from education where id=?educated");
                    while (rd.Read())
                        education.Add(Convert.ToInt32(educated_ids), rd.GetString("name"));
                }
                ViewBag.education = education;
                ViewBag.nrEducation = education.Count;

                try
                {
                    //get 3 series for an actor
                    Dictionary<int, similar> series = new Dictionary<int, similar>();

                    rd = db.ExecuteReader("select id,title,img, rating/nr_rating as total_rating from series where creator_ids like '%," + id + ",%' or composer_id like '%," + id + ",%' order by total_rating desc limit 3");
                    while (rd.Read())
                        series.Add(rd.GetInt32("id"), new similar(rd.GetString("img"), rd.GetString("title")));
                    ViewBag.similar = series;
                }
                catch(Exception e) { ViewBag.error = e.ToString(); }
               

                db.Close();
            }
            catch (Exception e)
            {
                ViewBag.error = e.ToString();
            }
            return View();
        }

        public IActionResult actor(int id)
        {
            try
            {
                ViewBag.path = "http://craigslist.gplay.ro/actors/";

                database db = new database(database.maindb);
                db.AddParam("?id", id);
                MySqlDataReader rd = db.ExecuteReader("select * from actors where id=?id");
                string awards_ids = "";
                string educated_ids = "";
                Dictionary<int, string> education = new Dictionary<int, string>();
                Dictionary<int, string> awards = new Dictionary<int, string>();

                while (rd.Read())
                {
                    ViewBag.name = rd.GetString("name");
                    ViewBag.description = rd.GetString("description");
                    ViewBag.subtitle = rd.GetString("subtitle");
                    ViewBag.wikipedia = rd.GetString("wikipedia");
                    ViewBag.img = rd.GetString("image");
                    ViewBag.date_of_birth = rd.GetString("date_of_birth");
                    ViewBag.gender = rd.GetInt32("gender");
                    ViewBag.date_of_death = rd.GetString("date_of_death");
                    ViewBag.place_birth = rd.GetString("place_birth");
                    ViewBag.imdb = rd.GetString("imdb");
                    awards_ids = rd.GetString("awards_id");
                    educated_ids = rd.GetString("educated_id");
                }
                if (awards_ids.Contains(","))
                {
                    awards_ids = awards_ids.Remove(0, 1);
                    string[] split = awards_ids.Split(',');
                    MySqlParameter awardPrm = db.Command.Parameters.Add("?award", MySqlDbType.Int32);
                    for (int i = 0; i < split.Length - 1; i++)
                    {
                        awardPrm.Value = Convert.ToInt32(split[i]);
                        rd = db.ExecuteReader("select award from awards where id=?award");
                        while (rd.Read())
                        {
                            awards.Add(Convert.ToInt32(split[i]), rd.GetString("award"));
                        }
                    }
                }
                else if (awards_ids != "")
                {
                    db.AddParam("?award", Convert.ToInt32(awards_ids));
                    rd = db.ExecuteReader("select award from awards where id=?award");
                    while (rd.Read())
                        awards.Add(Convert.ToInt32(awards_ids), rd.GetString("award"));
                }
                ViewBag.awards = awards;
                ViewBag.nrAwards = awards.Count;

                if (educated_ids.Contains(","))
                {
                    educated_ids = educated_ids.Remove(0, 1);
                    string[] split = educated_ids.Split(',');
                    MySqlParameter educatedPrm = db.Command.Parameters.Add("?educated", MySqlDbType.Int32);
                    for (int i = 0; i < split.Length - 1; i++)
                    {
                        educatedPrm.Value = Convert.ToInt32(split[i]);
                        rd = db.ExecuteReader("select name from education where id=?educated");
                        while (rd.Read())
                        {
                            education.Add(Convert.ToInt32(split[i]), rd.GetString("name"));
                        }
                    }
                }
                else if (educated_ids != "")
                {
                    db.AddParam("?educated", Convert.ToInt32(educated_ids));
                    rd = db.ExecuteReader("select name from education where id=?educated");
                    while (rd.Read())
                        education.Add(Convert.ToInt32(educated_ids), rd.GetString("name"));
                }
                ViewBag.education = education;
                ViewBag.nrEducation = education.Count;


                //get 3 series for an actor
                Dictionary<int, similar> series = new Dictionary<int, similar>();

                rd = db.ExecuteReader("select id,title,img, rating/nr_rating as total_rating from series where actor_ids like '%," + id + ",%' order by total_rating desc limit 3");
                while (rd.Read())
                    series.Add(rd.GetInt32("id"), new similar(rd.GetString("img"), rd.GetString("title")));
                ViewBag.similar = series;


                db.Close();
            }
            catch (Exception e)
            {
                ViewBag.error = e.ToString();
            }

            return View();
        }
        public IActionResult network(int id)
        {
            try
            {
                ViewBag.path = "http://craigslist.gplay.ro/series/";
                Dictionary<int, Series> series = new Dictionary<int, Series>();
                database db = new database(database.maindb);
                db.AddParam("?id", id);
                string description = "";

                Dictionary<int, string> genres = new Dictionary<int, string>();
                MySqlDataReader rd = db.ExecuteReader("select nr_rating,rating/nr_rating as total_rating,id,title,description,img,genre_id from series where network_id=?id limit 10");
                while (rd.Read())
                {
                    description = rd.GetString("description");
                    if (description.Length > 400)
                    {
                        description = description.Remove(400) + " ...";
                    }
                    string total_rating = "0";
                    if (rd.GetInt32("nr_rating") == 0)
                    { total_rating = "0"; }
                    else
                    {
                        total_rating = rd.GetDouble("total_rating").ToString();
                        if (total_rating.Count() > 3) total_rating = total_rating.Remove(total_rating.IndexOf('.') + 2);
                    }
                    series.Add(rd.GetInt32("id"), new Series(rd.GetString("title"), description, rd.GetString("img"), genres, rd.GetString("genre_id"), 0, "", total_rating));
                }
                int my_rating = 0;
                List<int> keys = new List<int>(series.Keys);
                if (Context.Session.GetInt32("on") == 1)
                {
                    db.AddParam("?id_user", Context.Session.GetInt32("id"));
                    MySqlParameter id_seriePrm = db.Command.Parameters.Add("?id_serie", MySqlDbType.Int32);
                    foreach (int key in keys)
                    {
                        id_seriePrm.Value = key;

                        try
                        {
                            rd = db.ExecuteReader("select rating from rating where id_user=?id_user and id_serie=?id_serie");
                            if (rd.HasRows)
                                while (rd.Read())
                                {
                                    my_rating = rd.GetInt32("rating");
                                    // serie.Value.my_rating[rd.GetInt32("rating")] = "checked";
                                    break;
                                }
                            else my_rating = 0;
                            series[key] = new Series(series[key].title, series[key].description, series[key].img, series[key].genres, series[key].genre_id, my_rating, "", series[key].total_rating);
                        }
                        catch (Exception e)
                        {
                            ViewBag.error = e.ToString();
                        }

                    }
                }


                rd = db.ExecuteReader("select count(*) as nr from series where network_id=?id");
                ViewBag.totalpages = 0;
                ViewBag.id = id;
                while (rd.Read())
                {
                    ViewBag.totalpages = rd.GetInt32("nr") / 10;
                }

                rd = db.ExecuteReader("select network from network where id=?id");
                while (rd.Read())
                {
                    ViewBag.title = UppercaseFirst(rd.GetString("network"));
                }


                ViewBag.series = series;




                db.Close();
            }
            catch (Exception e)
            {
                ViewBag.error = e.ToString();
            }

            return View();
        }
        public IActionResult award(int id)
        {
            try
            {
                ViewBag.path = "http://craigslist.gplay.ro/";
                Dictionary<int, Person> persons = new Dictionary<int, Person>();
                database db = new database(database.maindb);
                db.AddParam("?id", id);
                string description = "";

                ViewBag.id = id;
                MySqlDataReader rd = db.ExecuteReader("select id,name,description,image,subtitle from actors where awards_id like '%," + id + ",%' limit 10");
                while (rd.Read())
                {
                    description = rd.GetString("description");
                    if (description.Length > 400)
                    {
                        description = description.Remove(400) + " ...";
                    }
                    persons.Add(rd.GetInt32("id"), new Person(rd.GetString("name"), rd.GetString("image"), description, rd.GetString("subtitle"), 0));
                }

                rd = db.ExecuteReader("select count(*) as nr from actors where awards_id like '%," + id + ",%'");
                ViewBag.totalpages = 0;
                while (rd.Read())
                {
                    ViewBag.totalpages = rd.GetInt32("nr") / 10;
                }
                rd = db.ExecuteReader("select award from awards where id=?id");
                while (rd.Read())
                {
                    ViewBag.title = UppercaseFirst(rd.GetString("award"));
                }

                ViewBag.persons = persons;
                db.Close();
            }
            catch (Exception e)
            {
                ViewBag.error = e.ToString();
            }
            return View();
        }
        public IActionResult education(int id)
        {
            try
            {
                ViewBag.path = "http://craigslist.gplay.ro/";
                Dictionary<int, Person> persons = new Dictionary<int, Person>();
                database db = new database(database.maindb);
                db.AddParam("?id", id);
                string description = "";

                ViewBag.id = id;
                MySqlDataReader rd = db.ExecuteReader("select id,name,description,image,subtitle from actors where educated_id like '%," + id + ",%' limit 10");
                while (rd.Read())
                {
                    description = rd.GetString("description");
                    if (description.Length > 400)
                    {
                        description = description.Remove(400) + " ...";
                    }
                    persons.Add(rd.GetInt32("id"), new Person(rd.GetString("name"), rd.GetString("image"), description, rd.GetString("subtitle"), 0));
                }
                try
                {
                    if (persons.Count < 10)
                    {
                        int limit = 10 - persons.Count;
                        rd = db.ExecuteReader("select id,name,description,image,subtitle,type from creation where educated_id like '%," + id + ",%' limit " + limit);
                        while (rd.Read())
                        {
                            bool exists = persons.Any(b => (b.Value != null && b.Value.name == rd.GetString("name")));
                            if (exists == false)
                            {
                                description = rd.GetString("description");
                                if (description.Length > 400)
                                {
                                    description = description.Remove(400) + " ...";
                                }
                                persons.Add(rd.GetInt32("id"), new Person(rd.GetString("name"), rd.GetString("image"), description, rd.GetString("subtitle"), rd.GetInt32("type")));
                            }
                        }
                    }
                }
                catch (Exception e) { ViewBag.error = e.ToString(); }

                rd = db.ExecuteReader("select count(*) as nr from actors where educated_id like '%," + id + ",%'");
                ViewBag.totalpages = 0;
                while (rd.Read())
                {
                    ViewBag.totalpages = rd.GetInt32("nr");
                }
                rd = db.ExecuteReader("select count(*) as nr from creation where educated_id like '%," + id + ",%'");
                ViewBag.totalpages = 0;
                while (rd.Read())
                {
                    ViewBag.totalpages = ViewBag.totalpages + rd.GetInt32("nr");
                }
                ViewBag.totalpages = ViewBag.totalpages / 10;

                rd = db.ExecuteReader("select name from education where id=?id");
                while (rd.Read())
                {
                    ViewBag.title = UppercaseFirst(rd.GetString("name"));
                }

                ViewBag.persons = persons;
                db.Close();
            }
            catch (Exception e)
            {
                ViewBag.error = e.ToString();
            }
            return View();
        }

        public IActionResult language(int id)
        {
            try
            {
                ViewBag.path = "http://craigslist.gplay.ro/series/";
                Dictionary<int, Series> series = new Dictionary<int, Series>();
                database db = new database(database.maindb);
                db.AddParam("?id", id);
                string description = "";

                Dictionary<int, string> genres = new Dictionary<int, string>();
                MySqlDataReader rd = db.ExecuteReader("select nr_rating,rating/nr_rating as total_rating,id,title,description,img,genre_id from series where language_id like '%" + id + ",%' limit 10");
                while (rd.Read())
                {
                    description = rd.GetString("description");
                    if (description.Length > 400)
                    {
                        description = description.Remove(400) + " ...";
                    }
                    string total_rating = "0";
                    if (rd.GetInt32("nr_rating") == 0)
                    { total_rating = "0"; }
                    else
                    {
                        total_rating = rd.GetDouble("total_rating").ToString();
                        if (total_rating.Count() > 3) total_rating = total_rating.Remove(total_rating.IndexOf('.') + 2);
                    }
                    series.Add(rd.GetInt32("id"), new Series(rd.GetString("title"), description, rd.GetString("img"), genres, rd.GetString("genre_id"), 0, "", total_rating));
                }

                int my_rating = 0;
                List<int> keys = new List<int>(series.Keys);
                if (Context.Session.GetInt32("on") == 1)
                {
                    db.AddParam("?id_user", Context.Session.GetInt32("id"));
                    MySqlParameter id_seriePrm = db.Command.Parameters.Add("?id_serie", MySqlDbType.Int32);
                    foreach (int key in keys)
                    {
                        id_seriePrm.Value = key;

                        try
                        {
                            rd = db.ExecuteReader("select rating from rating where id_user=?id_user and id_serie=?id_serie");
                            if (rd.HasRows)
                                while (rd.Read())
                                {
                                    my_rating = rd.GetInt32("rating");
                                    // serie.Value.my_rating[rd.GetInt32("rating")] = "checked";
                                    break;
                                }
                            else my_rating = 0;
                            series[key] = new Series(series[key].title, series[key].description, series[key].img, series[key].genres, series[key].genre_id, my_rating, "", series[key].total_rating);
                        }
                        catch (Exception e)
                        {
                            ViewBag.error = e.ToString();
                        }

                    }
                }


                rd = db.ExecuteReader("select count(*) as nr from series where language_id like '%" + id + ",%'");
                ViewBag.totalpages = 0;
                ViewBag.id = id;
                while (rd.Read())
                {
                    ViewBag.totalpages = rd.GetInt32("nr") / 10;
                }

                rd = db.ExecuteReader("select language from languages where id=?id");
                while (rd.Read())
                {
                    ViewBag.title = UppercaseFirst(rd.GetString("language"));
                }


                ViewBag.series = series;




                db.Close();
            }
            catch (Exception e)
            {
                ViewBag.error = e.ToString();
            }
            return View();
        }

        [HttpPost]
        public string resultPagination(int start_from, string id, string type)
        {
            try
            {
                Dictionary<int, Series> series = new Dictionary<int, Series>();
                Dictionary<int, Person> persons = new Dictionary<int, Person>();
                List<int> id_series = new List<int>();
                database db = new database(database.maindb);
                db.AddParam("?start_from", start_from);
                db.AddParam("?id", id);


                string description = "";
                MySqlDataReader rd;
                Dictionary<int, string> genres = new Dictionary<int, string>();
                string query = "select nr_rating,rating/nr_rating as total_rating,id, title, description, img, genre_id from";


                if (type == "genre") query = query + " series where genre_id like '%," + id + ",%' limit ?start_from,10";
                else if (type == "country") query = query + " series where country_id=?id limit ?start_from,10";
                else if (type == "network") query = query + " series where network_id=?id limit ?start_from,10";
                else if (type == "person")
                {
                    query = "select id,name,description,image,subtitle from actors where educated_id like '%," + id + ",%' limit ?start_from,10";
                }
                else if (type == "language") query = query + " series where language_id like '%" + id + ",%' limit ?start_from,10";
                else if (type == "favorites")
                {
                    if (Context.Session.GetInt32("on") == 1)
                        db.AddParam("?id_user", Context.Session.GetInt32("id"));
                    {
                        query = "select * from favorites where id_user=?id_user and `option`=2 limit ?start_from,10";
                        rd = db.ExecuteReader(query);
                        while (rd.Read())
                            id_series.Add(rd.GetInt32("id_serie"));
                        MySqlParameter id_serie = db.Command.Parameters.Add("?id_serie", MySqlDbType.Int32);
                        for (int i = 0; i < id_series.Count; i++)
                        {
                            id_serie.Value = id_series[i];
                            rd = db.ExecuteReader("select nr_rating,rating/nr_rating as total_rating,description,id,img,title,genre_id from series where id=?id_serie");
                            while (rd.Read())
                            {
                                description = rd.GetString("description");
                                if (description.Length > 400)
                                {
                                    description = description.Remove(400) + " ...";
                                }
                                string total_rating = "0";
                                if (rd.GetInt32("nr_rating") == 0)
                                { total_rating = "0"; }
                                else
                                {
                                    total_rating = rd.GetDouble("total_rating").ToString();
                                    if (total_rating.Count() > 3) total_rating = total_rating.Remove(total_rating.IndexOf('.') + 2);
                                }
                                series.Add(rd.GetInt32("id"), new Series(rd.GetString("title"), description, rd.GetString("img"), genres, rd.GetString("genre_id"), 0, "", total_rating));
                            }
                            int my_rating = 0;
                            List<int> keys = new List<int>(series.Keys);
                            if (Context.Session.GetInt32("on") == 1)
                            {
                                
                                foreach (int key in keys)
                                {
                                    id_serie.Value = key;

                                    try
                                    {
                                        rd = db.ExecuteReader("select rating from rating where id_user=?id_user and id_serie=?id_serie");
                                        if (rd.HasRows)
                                            while (rd.Read())
                                            {
                                                my_rating = rd.GetInt32("rating");
                                                // serie.Value.my_rating[rd.GetInt32("rating")] = "checked";
                                                break;
                                            }
                                        else my_rating = 0;
                                        series[key] = new Series(series[key].title, series[key].description, series[key].img, series[key].genres, series[key].genre_id, my_rating, "", series[key].total_rating);
                                    }
                                    catch (Exception e)
                                    {
                                        ViewBag.error = e.ToString();
                                    }

                                }
                            }

                            }
                    }
                }

                if (type != "favorites")
                {
                    rd = db.ExecuteReader(query);
                    while (rd.Read())
                    {
                        description = rd.GetString("description");
                        if (description.Length > 400)
                        {

                            description = description.Remove(400) + " ...";
                        }
                        string total_rating = "0";
                        if (rd.GetInt32("nr_rating") == 0)
                        { total_rating = "0"; }
                        else
                        {
                            total_rating = rd.GetDouble("total_rating").ToString();
                            if (total_rating.Count() > 3) total_rating = total_rating.Remove(total_rating.IndexOf('.') + 2);
                        }

                        if (type == "person") persons.Add(rd.GetInt32("id"), new Person(rd.GetString("name"), rd.GetString("image"), description, rd.GetString("subtitle"), 0));
                        else series.Add(rd.GetInt32("id"), new Series(rd.GetString("title"), description, rd.GetString("img"), genres, rd.GetString("genre_id"), 0, "", total_rating));
                    }

                    int my_rating = 0;
                    List<int> keys = new List<int>(series.Keys);
                    if (Context.Session.GetInt32("on") == 1)
                    {
                        db.AddParam("?id_user", Context.Session.GetInt32("id"));
                        MySqlParameter id_seriePrm = db.Command.Parameters.Add("?id_serie", MySqlDbType.Int32);
                        foreach (int key in keys)
                        {
                            id_seriePrm.Value = key;

                            try
                            {
                                rd = db.ExecuteReader("select rating from rating where id_user=?id_user and id_serie=?id_serie");
                                if (rd.HasRows)
                                    while (rd.Read())
                                    {
                                        my_rating = rd.GetInt32("rating");
                                        // serie.Value.my_rating[rd.GetInt32("rating")] = "checked";
                                        break;
                                    }
                                else my_rating = 0;
                                series[key] = new Series(series[key].title, series[key].description, series[key].img, series[key].genres, series[key].genre_id, my_rating, "", series[key].total_rating);
                            }
                            catch (Exception e)
                            {
                                ViewBag.error = e.ToString();
                            }

                        }
                    }

                }
                db.Close();
                if (type == "person") return JsonConvert.SerializeObject(persons);
                return JsonConvert.SerializeObject(series);
            }
            catch (Exception e)
            {
                return e.ToString();
            }
            //return "0";
        }

        static string UppercaseFirst(string s)
        {
            // Check for empty string.
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            // Return char and concat substring.
            return char.ToUpper(s[0]) + s.Substring(1);
        }

        [HttpPost]
        public string addComment(string comment, int id)
        {
            try
            {
                database db = new database(database.maindb);
                db.AddParam("?comment", comment);
                db.AddParam("?id_serie", id);
                int id_user = Convert.ToInt32(Context.Session.GetInt32("id"));
                db.AddParam("?id_user", id_user);
                string date = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                db.AddParam("?date", date);
                db.ExecuteNonQuery("insert into comments(id_serie,id_user,comment,date) values(?id_serie,?id_user,?comment,NOW())");
                db.Close();
                return date;
            }
            catch
            {
                return "-1";
            }

        }

        [HttpPost]
        public string addFavorites(int option, int id)
        {
            try
            {
                database db = new database(database.maindb);
                db.AddParam("?id_serie", id);
                db.AddParam("?option", option);
                db.AddParam("?id", Context.Session.GetInt32("id"));

                MySqlDataReader rd = db.ExecuteReader("select * from favorites where id_serie=?id_serie and id_user=?id");
                if (rd.HasRows)
                {
                    db.ExecuteNonQuery("update favorites set `option`=?option where id_serie=?id_serie and id_user=?id");

                }
                else
                    db.ExecuteNonQuery("insert into favorites(id_user,id_serie,`option`) values(?id,?id_serie,?option)");
                return option.ToString();
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        public IActionResult Favorites()
        {
            try
            {
                ViewBag.path = "http://craigslist.gplay.ro/series/";
                Dictionary<int, Series> series = new Dictionary<int, Series>();
                database db = new database(database.maindb);
                string description = "";
                List<int> id_series = new List<int>();
                Dictionary<int, string> genres = new Dictionary<int, string>();
                db.AddParam("?id", Context.Session.GetInt32("id"));
                string query = "select * from favorites where id_user=?id and `option`=2 ";

                MySqlDataReader rd = db.ExecuteReader(query);
                if (!rd.HasRows)
                    ViewBag.totalpages = 0;
                while (rd.Read())
                    id_series.Add(rd.GetInt32("id_serie"));

                if (id_series.Count % 10 == 0)
                    ViewBag.totalpages = id_series.Count / 10;
                else ViewBag.totalpages = id_series.Count / 10 + 1;
                int nr = id_series.Count;
                MySqlParameter id_serie = db.Command.Parameters.Add("?id_serie", MySqlDbType.Int32);
                if (nr > 10) nr = 10;
                for (int i = 0; i < nr; i++)
                {
                    id_serie.Value = id_series[i];
                    rd = db.ExecuteReader("select nr_rating,rating/nr_rating as total_rating, description,id,img,title,genre_id from series where id=?id_serie");
                    while (rd.Read())
                    {
                        description = rd.GetString("description");
                        if (description.Length > 400)
                        {
                            description = description.Remove(400) + " ...";
                        }
                        string total_rating = "0";
                        if (rd.GetInt32("nr_rating") == 0)
                        { total_rating = "0"; }
                        else
                        {
                            total_rating = rd.GetDouble("total_rating").ToString();
                            if (total_rating.Count() > 3) total_rating = total_rating.Remove(total_rating.IndexOf('.') + 2);
                        }
                        series.Add(rd.GetInt32("id"), new Series(rd.GetString("title"), description, rd.GetString("img"), genres, rd.GetString("genre_id"), 0, "", total_rating));
                    }

                }

                int my_rating = 0;
                List<int> keys = new List<int>(series.Keys);
                if (Context.Session.GetInt32("on") == 1)
                {
                    db.AddParam("?id_user", Context.Session.GetInt32("id"));

                    foreach (int key in keys)
                    {
                        id_serie.Value = key;
                        rd = db.ExecuteReader("select rating from rating where id_user=?id_user and id_serie=?id_serie");
                        if (rd.HasRows)
                            while (rd.Read())
                            {
                                my_rating = rd.GetInt32("rating");
                                // serie.Value.my_rating[rd.GetInt32("rating")] = "checked";
                                break;
                            }
                        else my_rating = 0;
                        series[key] = new Series(series[key].title, series[key].description, series[key].img, series[key].genres, series[key].genre_id, my_rating, "", series[key].total_rating);



                    }
                }

                ViewBag.series = series;




                db.Close();
            }
            catch (Exception e)
            {
                ViewBag.error = e.ToString();
            }
            return View();


        }
        [HttpPost]
        public string Rating(int rating, int id)
        {
            try
            {
                string total_rating = "";
                int old_rating = 0;
                database db = new database(database.maindb);
                db.AddParam("?id_user", Context.Session.GetInt32("id"));
                db.AddParam("?id_serie", id);
                db.AddParam("?rating", rating);
                MySqlDataReader rd = db.ExecuteReader("select * from rating where id_user=?id_user and id_serie=?id_serie");
                if (rd.HasRows)
                {
                    while (rd.Read())
                        old_rating = rd.GetInt32("rating");
                    db.AddParam("?old_rating", old_rating);
                    db.ExecuteNonQuery("update rating set rating=?rating where id_user=?id_user and id_serie=?id_serie");
                    db.ExecuteNonQuery("update series set rating=rating+?rating-?old_rating where id=?id_serie");

                }
                else
                {
                    db.ExecuteNonQuery("insert into rating(id_user,id_serie,rating) values(?id_user,?id_serie,?rating)");
                    db.ExecuteNonQuery("update series set rating=rating+?rating,nr_rating=nr_rating+1 where id=?id_serie");
                }

                rd = db.ExecuteReader("select (rating/nr_rating) as total_rating from series where id=?id_serie");
                while (rd.Read())
                    total_rating = rd.GetDouble("total_rating").ToString();
                if (total_rating.Count() > 3) total_rating = total_rating.Remove(total_rating.IndexOf('.') + 2);
                db.Close();
                return total_rating;
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        public IActionResult top()
        {
            try
            {

                string favorites = "Add to favorites";
                database db = new database(database.maindb);

                MySqlDataReader rd = db.ExecuteReader("SELECT id,title,img,rating/nr_rating as serie_rating,nr_rating FROM `series` order by serie_rating desc limit 100");
                Dictionary<int, TopSerie> top = new Dictionary<int, TopSerie>();
                int my_rating = 0;

                while (rd.Read())
                {
                    string total_rating = "0";
                    if (rd.GetInt32("nr_rating") == 0)
                    { total_rating = "0"; }
                    else
                    {
                        total_rating = rd.GetDouble("serie_rating").ToString();
                        if (total_rating.Count() > 3) total_rating = total_rating.Remove(total_rating.IndexOf('.') + 2);
                    }
                    top.Add(rd.GetInt32("id"), new TopSerie(rd.GetString("title"), rd.GetString("img"), Convert.ToDouble(total_rating), my_rating, "Add to favorites", rd.GetInt32("nr_rating")));
                }
                List<int> keys = new List<int>(top.Keys);
                if (Context.Session.GetInt32("on") == 1)
                {
                    db.AddParam("?id_user", Context.Session.GetInt32("id"));
                    MySqlParameter id_seriePrm = db.Command.Parameters.Add("?id_serie", MySqlDbType.Int32);
                    foreach (int key in keys)
                    {
                        id_seriePrm.Value = key;

                        try
                        {
                            rd = db.ExecuteReader("select rating from rating where id_user=?id_user and id_serie=?id_serie");
                            if (rd.HasRows)
                                while (rd.Read())
                                {
                                    my_rating = rd.GetInt32("rating");
                                    // serie.Value.my_rating[rd.GetInt32("rating")] = "checked";
                                    break;
                                }
                            else my_rating = 0;
                            rd = db.ExecuteReader("select `option` from favorites where id_user=?id_user and id_serie=" + key);
                            if (rd.HasRows)
                                while (rd.Read())
                                    if (rd.GetInt32("option") == 2)
                                        favorites = "Remove from favorites";
                                    else favorites = "Add to favorites";
                            else favorites = "Add to favorites";
                            top[key] = new TopSerie(top[key].title, top[key].img, top[key].rating, my_rating, favorites, top[key].nr_votes);
                        }
                        catch (Exception e)
                        {
                            ViewBag.error = e.ToString();
                        }

                    }
                }
                ViewBag.top = top;
            }
            catch (Exception e)
            {
                ViewBag.error = e.ToString();
            }
            return View();
        }

        [HttpPost]
        public string Season(int id, string season, string imdb)
        {
            try
            {
                database db = new database(database.maindb);
                MySqlDataReader rd;
                db.AddParam("?season", season);
                db.AddParam("?id_serie", id);
                if (Context.Session.GetInt32("on") == 1)
                    db.AddParam("?id_user", Context.Session.GetInt32("id"));

                List<Episode> episodes = new List<Episode>();
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://api.tvmaze.com/lookup/shows?imdb=" + imdb);

                request.AllowAutoRedirect = false;
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                string redirUrl = response.Headers["Location"] + "/episodes";
                string content = "";
                using (WebClient wc = new WebClient())
                {
                    content = wc.DownloadString(redirUrl);
                }
                response.Close();

                JArray jsonInfo = JArray.Parse(content);
                var SeasonEpisodes = jsonInfo.Where(x => x["season"].ToString() == season).ToList();
                int watched = 0;

                string img_episode = "";
                for (int i = 0; i < SeasonEpisodes.Count; i++)
                {
                    if (Context.Session.GetInt32("on") == 1)
                    {
                        int nr_episode = Convert.ToInt32(SeasonEpisodes[i]["number"]);
                        rd = db.ExecuteReader("select * from watched where season=?season and id_user=?id_user and id_serie=?id_serie and episodes like '%," + nr_episode + "%'");
                        if (rd.HasRows)
                            watched = 1;
                        else watched = 0;
                    }
                    try
                    {
                        img_episode = SeasonEpisodes[i]["image"]["medium"].ToString();
                    }
                    catch { }
                    if (img_episode == "") img_episode = "../images/series/episode_noimage.png";
                    episodes.Add(new Episode(SeasonEpisodes[i]["name"].ToString(), Convert.ToInt32(SeasonEpisodes[i]["number"]), SeasonEpisodes[i]["airdate"].ToString(),
                        img_episode, SeasonEpisodes[i]["summary"].ToString(), watched));
                }

                return JsonConvert.SerializeObject(episodes);
            }
            catch { return "0"; }
        }

        public IActionResult topseries(string type, int id)
        {
            try
            {
                database db = new database(database.maindb);
                string query = "", query2 = "";
                db.AddParam("?type", type);
                db.AddParam("?id", id);
                string favorites = "Add to favorites";
                Dictionary<int, TopSerie> top = new Dictionary<int, TopSerie>();
                int my_rating = 0;
                MySqlDataReader rd;

                if (type == "genre")
                {
                    query = "SELECT id,title,img,rating/nr_rating as serie_rating,nr_rating FROM `series` where genre_id like '%," + id + ",%' order by serie_rating desc limit 100";
                    query2 = "select genre as title from genres where id=?id";
                }
                if (type == "language")
                {
                    query = "SELECT id,title,img,rating/nr_rating as serie_rating,nr_rating FROM `series` where language_id=?id order by serie_rating desc limit 100";
                    query2 = "select language as title from languages where id=?id";
                }
                if (type == "country")
                {
                    query = "SELECT id,title,img,rating/nr_rating as serie_rating,nr_rating FROM `series` where country_id=?id order by serie_rating desc limit 100";
                    query2 = "select country as title from countries where id=?id";
                }
                if (type == "network")
                {
                    query = "SELECT id,title,img,rating/nr_rating as serie_rating,nr_rating FROM `series` where network_id=?id order by serie_rating desc limit 100";
                    query2 = "select network as title from network where id=?id";
                }
                rd = db.ExecuteReader(query);
                while (rd.Read())
                {
                    string total_rating = "0";
                    if (rd.GetInt32("nr_rating") == 0)
                    { total_rating = "0"; }
                    else
                    {
                        total_rating = rd.GetDouble("serie_rating").ToString();
                        if (total_rating.Count() > 3) total_rating = total_rating.Remove(total_rating.IndexOf('.') + 2);
                    }
                    //verify if image is available
                   
                    top.Add(rd.GetInt32("id"), new TopSerie(rd.GetString("title"), rd.GetString("img"), Convert.ToDouble(total_rating), my_rating, "Add to favorites", rd.GetInt32("nr_rating")));
                }
                List<int> keys = new List<int>(top.Keys);
                if (Context.Session.GetInt32("on") == 1)
                {
                    db.AddParam("?id_user", Context.Session.GetInt32("id"));
                    MySqlParameter id_seriePrm = db.Command.Parameters.Add("?id_serie", MySqlDbType.Int32);
                    foreach (int key in keys)
                    {
                        id_seriePrm.Value = key;

                        try
                        {
                            rd = db.ExecuteReader("select rating from rating where id_user=?id_user and id_serie=?id_serie");
                            if (rd.HasRows)
                                while (rd.Read())
                                {
                                    my_rating = rd.GetInt32("rating");
                                    // serie.Value.my_rating[rd.GetInt32("rating")] = "checked";
                                    break;
                                }
                            else my_rating = 0;
                            rd = db.ExecuteReader("select `option` from favorites where id_user=?id_user and id_serie=" + key);
                            if (rd.HasRows)
                                while (rd.Read())
                                    if (rd.GetInt32("option") == 2)
                                        favorites = "Remove from favorites";
                                    else favorites = "Add to favorites";
                            else favorites = "Add to favorites";
                            top[key] = new TopSerie(top[key].title, top[key].img, top[key].rating, my_rating, favorites, top[key].nr_votes);
                        }
                        catch (Exception e)
                        {
                            ViewBag.error = e.ToString();
                        }

                    }
                }

                rd = db.ExecuteReader(query2);
                while (rd.Read())
                    ViewBag.title = UppercaseFirst(rd.GetString("title"));
                ViewBag.top = top;

                db.Close();
            }
            catch { }
            return View();
        }

        [HttpPost]
        public string Watched(string is_checked, int id, int season, int episode)
        {
            try
            {
                database db = new database(database.maindb);
                db.AddParam("?id_user", Context.Session.GetInt32("id"));
                db.AddParam("?id_serie", id);
                db.AddParam("?season", season);
                MySqlDataReader rd;

                if (is_checked == "true")
                {
                    rd = db.ExecuteReader("select * from watched where id_serie=?id_serie and id_user=?id_user and season=?season");
                    if (rd.HasRows)
                        while (rd.Read())
                        {
                            db.AddParam("?episode", episode + ",");
                            db.ExecuteNonQuery("update watched set episodes=concat(episodes,?episode) where id_serie=?id_serie and id_user=?id_user and season=?season");
                        }
                    else
                    {
                        db.AddParam("?episode", "," + episode + ",");
                        db.ExecuteNonQuery("insert into watched(id_user,id_serie,season,episodes) values (?id_user,?id_serie,?season,?episode)");
                    }
                }
                else
                {
                    db.AddParam("?episode", "," + episode);
                    db.ExecuteNonQuery("update watched set episodes=replace(episodes,?episode,'') where id_serie=?id_serie and id_user=?id_user and season=?season");
                    rd = db.ExecuteReader("delete from watched where episodes=','");
                }
                db.Close();
                return "1";
            }
            catch
            {
                return "0";
            }
        }

        public IActionResult Episodes()
        {

            try
            {
                database db = new database(database.maindb);
                string today_date = DateTime.Now.ToString("yyyy-MM-dd");
                ViewBag.date = today_date;
                db.AddParam("?today_date", today_date);
                Dictionary<int, WeekEpisodes> episodes = new Dictionary<int, WeekEpisodes>();


                MySqlDataReader rd = db.ExecuteReader("select id,rating/nr_rating as total_rating,title,img,next_episode_number from series where next_episode_airdate = ?today_date and img!='' order by total_rating desc");
                while (rd.Read())
                {
                    episodes.Add(rd.GetInt32("id"), new WeekEpisodes(rd.GetString("img"), rd.GetString("title"), rd.GetString("next_episode_number")));
                }
                ViewBag.today_episodes = episodes;
                ViewBag.picture = Startup.settings["series"];


                string next_day = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd");
                db.AddParam("?next_day", next_day);
                episodes = new Dictionary<int, WeekEpisodes>();
                rd = db.ExecuteReader("select id,rating/nr_rating as total_rating,title,img,next_episode_number from series where next_episode_airdate = ?next_day and img!='' order by total_rating desc");
                while (rd.Read())
                {
                    episodes.Add(rd.GetInt32("id"), new WeekEpisodes(rd.GetString("img"), rd.GetString("title"), rd.GetString("next_episode_number")));
                }
                ViewBag.tomorrow_episodes = episodes;

                string yesterday = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");
                db.AddParam("?yesterday", yesterday);
                episodes = new Dictionary<int, WeekEpisodes>();
                rd = db.ExecuteReader("select id,rating/nr_rating as total_rating,title,img,next_episode_number from series where next_episode_airdate = ?yesterday and img!='' order by total_rating desc");
                while (rd.Read())
                {
                    episodes.Add(rd.GetInt32("id"), new WeekEpisodes(rd.GetString("img"), rd.GetString("title"), rd.GetString("next_episode_number")));
                }
                ViewBag.yesterday_episodes = episodes;
                db.Close();
            }
            catch
            { }
            return View();
        }
    }
}
