﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using TVSeries.Models;
using System.Text;
using MySql.Data.MySqlClient;
using Microsoft.AspNet.Session;
//using System.Web;
using Microsoft.AspNet.Http;
using Newtonsoft.Json;
using System.Net;
using System.IO;
using System.Web;
using System.Net.Http.Headers;
using Microsoft.Framework.Runtime;

namespace TVSeries.Controllers
{
    public class HomeController : Controller
    {
        IApplicationEnvironment _hostingEnvironment;


        public HomeController(IApplicationEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        public IActionResult Index()
        {

            try
            {
                database db = new database(database.maindb);
                string today_date = DateTime.Now.ToString("yyyy-MM-dd");
                ViewBag.date = today_date;
                db.AddParam("?today_date", today_date);
                Dictionary<int, WeekEpisodes> episodes = new Dictionary<int, WeekEpisodes>();


                MySqlDataReader rd = db.ExecuteReader("select id,rating/nr_rating as total_rating,title,img,next_episode_number from series where next_episode_airdate = ?today_date and img!='' order by total_rating desc limit 10");
                while (rd.Read())
                {
                    episodes.Add(rd.GetInt32("id"), new WeekEpisodes(rd.GetString("img"), rd.GetString("title"), rd.GetString("next_episode_number")));
                }
                ViewBag.today_episodes = episodes;
                ViewBag.picture = Startup.settings["series"];


                string next_day = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd");
                db.AddParam("?next_day", next_day);
                episodes = new Dictionary<int, WeekEpisodes>();
                rd = db.ExecuteReader("select id,rating/nr_rating as total_rating,title,img,next_episode_number from series where next_episode_airdate = ?next_day and img!='' order by total_rating desc limit 10");
                while (rd.Read())
                {
                    episodes.Add(rd.GetInt32("id"), new WeekEpisodes(rd.GetString("img"), rd.GetString("title"), rd.GetString("next_episode_number")));
                }
                ViewBag.tomorrow_episodes = episodes;
                db.Close();
            }
            catch { }
            return View();

        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View("~/Views/Shared/Error.cshtml");
        }

        public IActionResult Register()
        {
            return View();
        }


        [HttpPost]
        public string RegisterUser(string email, string password, string firstname, string lastname, int gender, string country)
        {
            try
            {

                database db = new database(database.maindb);

                if (String.IsNullOrEmpty(firstname)) firstname = "";
                if (String.IsNullOrEmpty(lastname)) lastname = "";
                if (String.IsNullOrEmpty(country)) country = "";

                db.AddParam("?email", email);
                db.AddParam("?password", password);
                db.AddParam("?firstname", firstname);
                db.AddParam("?lastname", lastname);
                db.AddParam("?gender", gender);
                db.AddParam("?country", country);
                MySqlDataReader rd = db.ExecuteReader("select * from users where email=?email");
                if (rd.HasRows) return "-1"; // this email already exists
                db.ExecuteNonQuery("insert into users(email,password,firstname,lastname,gender,country) values(?email,?password,?firstname,?lastname,?gender,?country)");
                db.Close();

            }
            catch (Exception e)
            {
                return e.ToString();
            }
            return "1";
        }

        bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        [HttpPost]
        public string Login(string email, string password)
        {

            //-1 invalid email, -2 password or email doesnt exist
            try
            {
                if (!IsValidEmail(email)) return "-1";

                database db = new database(database.maindb);
                db.AddParam("?email", email);
                db.AddParam("?password", password);
                MySqlDataReader rd = db.ExecuteReader("select * from users where email=?email and password=?password");
                if (!rd.HasRows)
                {
                    Context.Session.SetInt32("on", 0);
                    // invalid user / pass
                    db.Close();
                    return "-2";
                }
                while (rd.Read())
                {
                    Context.Session.SetInt32("on", 1);
                    Context.Session.SetInt32("id", rd.GetInt32("id"));
                    Context.Session.SetString("email", rd.GetString("email"));
                    Context.Session.SetString("firstname", rd.GetString("firstname"));
                    Context.Session.SetString("name", rd.GetString("firstname") + " " + rd.GetString("lastname"));
                    Context.Session.SetString("lastname", rd.GetString("lastname"));
                    Context.Session.SetString("password", rd.GetString("password"));
                    Context.Session.SetString("country", rd.GetString("country"));
                    Context.Session.SetInt32("gender", rd.GetInt32("gender"));
                    break;
                }
                db.Close();
            }
            catch (Exception e)
            {
                //  HttpContext.Current.Session["on"] = 0;
                return e.ToString();
            }
            return "1";
        }

        [HttpPost]
        public string Logout()
        {
            Context.Session.SetInt32("on", 0);
            return "1";
        }



        [HttpPost]
        public string FBLogin(string token)
        {
            try
            {
                WebClient wc = new WebClient();
                wc.Proxy = null;


                string res = wc.DownloadString("https://graph.facebook.com/me?fields=email,name,first_name,last_name,gender&access_token=" + token);
                Dictionary<string, string> response = JsonConvert.DeserializeObject<Dictionary<string, string>>(res);
                if (response.ContainsKey("name"))
                {
                    string id = response["id"];
                    string email = response["email"];
                    database db = new database(database.maindb);
                    db.AddParam("?fbid", id);
                    db.AddParam("?email", email);
                    db.AddParam("?firstname", response["first_name"]);
                    db.AddParam("?lastname", response["last_name"]);

                    switch (response["gender"])
                    {
                        case "male":
                            db.AddParam("?gender", 1);
                            break;
                        case "female":
                            db.AddParam("?gender", 2);
                            break;
                        default:
                            db.AddParam("?gender", 0);
                            break;
                    }

                    MySqlDataReader rd;

                    rd = db.ExecuteReader("select * from users where facebookid = ?fbid or email = ?email");

                    if (rd.HasRows)
                        while (rd.Read())
                        {
                            Context.Session.SetInt32("on", 1);
                            Context.Session.SetString("email", rd.GetString("email"));
                            Context.Session.SetString("firstname", rd.GetString("firstname"));
                            Context.Session.SetString("lastname", rd.GetString("lastname"));
                            Context.Session.SetInt32("id", rd.GetInt32("id"));
                            Context.Session.SetString("myname", rd.GetString("lastname") + " " + rd.GetString("firstname"));
                            break;
                            // return "2";
                        }
                    else
                    {
                        db.ExecuteNonQuery("insert into users (email,firstname,lastname,gender,facebookid) values (?email,?firstname,?lastname,?gender,?fbid)");
                        rd = db.ExecuteReader("select * from users where facebookid = ?fbid or email = ?email");
                        while (rd.Read())
                        {
                            Context.Session.SetInt32("on", 1);
                            Context.Session.SetString("email", rd.GetString("email"));
                            Context.Session.SetString("firstname", rd.GetString("firstname"));
                            Context.Session.SetString("lastname", rd.GetString("lastname"));
                            Context.Session.SetInt32("id", rd.GetInt32("id"));
                            Context.Session.SetString("myname", rd.GetString("lastname") + " " + rd.GetString("firstname"));
                        }
                        //return "3";
                    }

                    db.Close();

                    return "1";
                }
                else
                    return "0";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }


        public IActionResult members()
        {
            try
            {
                database db = new database(database.maindb);
                Dictionary<int, User> users = new Dictionary<int, Models.User>();
                MySqlDataReader rd = db.ExecuteReader("select * from users limit 10");
                while (rd.Read())
                {
                    string gender;
                    if (rd.GetInt32("gender") == 1) gender = "male";
                    else if (rd.GetInt32("gender") == 2) gender = "female";
                    else gender = "unknown";
                    users.Add(rd.GetInt32("id"), new Models.User(rd.GetString("firstname") + " " + rd.GetString("lastname"), gender, rd.GetString("country"), rd.GetDateTime("joined"), 0));
                }
                rd = db.ExecuteReader("select count(*) as nr from users");
                while (rd.Read())
                    ViewBag.nrUsers = rd.GetInt32("nr");
                MySqlParameter id_friend = db.Command.Parameters.Add("?id_friend", MySqlDbType.Int32);
                if (Context.Session.GetInt32("on") == 1)
                {

                    db.AddParam("?myid", Context.Session.GetInt32("on"));
                    foreach (KeyValuePair<int, User> user in users)
                    {
                        if (user.Key != Context.Session.GetInt32("on"))
                        {

                            id_friend.Value = user.Key;
                            rd = db.ExecuteReader("select * from friends where (id_user1=?myid and id_user2=?id_friend) or(id_user1=?id_friend and id_user2=?myid)");
                            if (rd.HasRows)

                                user.Value.isFriend = 1;
                        }
                    }
                }
                ViewBag.users = users;
                db.Close();
            }
            catch { }
            return View();
        }
        [HttpPost]
        public string addFriend(int id_friend, int option)
        {
            try
            {
                database db = new database(database.maindb);
                db.AddParam("?id_friend", id_friend);
                db.AddParam("?id", Context.Session.GetInt32("id"));
                if (option == 1)
                    db.ExecuteNonQuery("insert into friends(id_user1,id_user2) values(?id_friend,?id)");
                else db.ExecuteNonQuery("delete from friends where (id_user1=?id and id_user2=?id_friend) or(id_user1=?id_friend and id_user2=?id)");
                db.Close();
                return "1";
            }
            catch (Exception e) { return e.ToString(); }

        }


        public IActionResult Profile()
        {

            try
            {
                if (Context.Session.GetInt32("on") == 1)
                {

                    ViewBag.firstname = Context.Session.GetString("firstname");
                    ViewBag.id = Context.Session.GetInt32("id");
                    ViewBag.lastname = Context.Session.GetString("lastname");
                    ViewBag.password = Context.Session.GetString("password");
                    ViewBag.country = Context.Session.GetString("country");
                    ViewBag.gender = Context.Session.GetInt32("gender");
                    ViewBag.email = Context.Session.GetString("email");
                    string image_path = _hostingEnvironment.ApplicationBasePath + "\\wwwroot\\images\\users\\" + ViewBag.id + ".png";
                    if (System.IO.File.Exists(image_path))
                        ViewBag.imageExists = 1;
                    else ViewBag.imageExists = 0;

                }

            }
            catch { }

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> UploadPicture(ICollection<IFormFile> files)
        {
            foreach (var file in files)
            {
                if (file.Length > 0)
                {
                    var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    await file.SaveAsAsync(_hostingEnvironment.ApplicationBasePath.Replace("\\approot\\src\\Movies", "") + "\\wwwroot\\images\\users\\" + Context.Session.GetInt32("id") + ".png");
                }
            }

            return Redirect("/Profile");
        }

        [HttpPost]
        public string UpdateProfile(string password, string firstname, string lastname, int gender, string country)
        {
            try
            {
                database db = new database(database.maindb);
                db.AddParam("?id", Context.Session.GetInt32("id"));
                db.AddParam("?password", password);
                db.AddParam("?firstname", firstname);
                db.AddParam("?lastname", lastname);
                db.AddParam("?gender", gender);
                db.AddParam("?country", country);
                db.ExecuteNonQuery("update users set password=?password,firstname=?firstname,lastname=?lastname,gender=?gender,country=?country where id=?id");

                db.Close();
                return "1";
            }
            catch 
            {
                return "0";
            }
        }



    }


}
