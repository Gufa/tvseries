﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Authentication.Facebook;
using Microsoft.AspNet.Authentication.Google;
using Microsoft.AspNet.Authentication.MicrosoftAccount;
using Microsoft.AspNet.Authentication.Twitter;
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Diagnostics;
using Microsoft.AspNet.Diagnostics.Entity;
using Microsoft.AspNet.Hosting;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Routing;
using Microsoft.Data.Entity;
using Microsoft.Framework.Configuration;
using Microsoft.Framework.DependencyInjection;
using Microsoft.Framework.Logging;
using Microsoft.Framework.Logging.Console;
using Microsoft.Framework.Runtime;
using TVSeries.Models;
using MySql.Data.MySqlClient;

namespace TVSeries
{
    public class Startup
    {
        public static Dictionary<string, string> settings = new Dictionary<string, string>();
        public Startup(IHostingEnvironment env, IApplicationEnvironment appEnv)
        {
            // Setup configuration sources.

            var builder = new ConfigurationBuilder(appEnv.ApplicationBasePath)
                .AddJsonFile("config.json")
                .AddJsonFile($"config.{env.EnvironmentName}.json", optional: true);

            if (env.IsDevelopment())
            {
                // This reads the configuration keys from the secret store.
                // For more details on using the user secret store see http://go.microsoft.com/fwlink/?LinkID=532709
                builder.AddUserSecrets();
            }
            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfiguration Configuration { get; set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
          

            // Configure the options for the authentication middleware.
            // You can add options for Google, Twitter and other middleware as shown below.
            // For more information see http://go.microsoft.com/fwlink/?LinkID=532715
            services.Configure<FacebookAuthenticationOptions>(options =>
            {
                options.AppId = Configuration["Authentication:Facebook:AppId"];
                options.AppSecret = Configuration["Authentication:Facebook:AppSecret"];
            });

            services.Configure<MicrosoftAccountAuthenticationOptions>(options =>
            {
                options.ClientId = Configuration["Authentication:MicrosoftAccount:ClientId"];
                options.ClientSecret = Configuration["Authentication:MicrosoftAccount:ClientSecret"];
            });

            // Add MVC services to the services container.
            services.AddMvc();

            // Uncomment the following line to add Web API services which makes it easier to port Web API 2 controllers.
            // You will also need to add the Microsoft.AspNet.Mvc.WebApiCompatShim package to the 'dependencies' section of project.json.
            // services.AddWebApiConventions();

            // Register application services.
            services.AddCaching();
           
            services.AddSession(options => {
                options.IdleTimeout = TimeSpan.FromMinutes(30);
               
            });
        }

        // Configure is called after ConfigureServices is called.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.MinimumLevel = LogLevel.Information;
            loggerFactory.AddConsole();
            app.UseSession();
            database db = new database(database.maindb);
            MySqlDataReader rd= db.ExecuteReader("select * from settings");
            while(rd.Read())
            {
                settings.Add(rd.GetString("name"), rd.GetString("value"));
            }
            db.Close();
            // Configure the HTTP request pipeline.

            // Add the following to the request pipeline only in development environment.
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseErrorPage(ErrorPageOptions.ShowAll);
                app.UseDatabaseErrorPage(DatabaseErrorPageOptions.ShowAll);
            }
            else
            {
                // Add Error handling middleware which catches all application specific errors and
                // sends the request to the following path or controller action.
                app.UseErrorHandler("/Home/Error");
            }

            // Add static files to the request pipeline.
            app.UseStaticFiles();

            // Add cookie-based authentication to the request pipeline.
            app.UseIdentity();

            // Add authentication middleware to the request pipeline. You can configure options such as Id and Secret in the ConfigureServices method.
            // For more information see http://go.microsoft.com/fwlink/?LinkID=532715
            // app.UseFacebookAuthentication();
            // app.UseGoogleAuthentication();
            // app.UseMicrosoftAccountAuthentication();
            // app.UseTwitterAuthentication();

            // Add MVC to the request pipeline.
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");

                // Uncomment the following line to add a route for porting Web API 2 controllers.
                // routes.MapWebApiRoute("DefaultApi", "api/{controller}/{id?}");
                routes.MapRoute("Register", "Register", new { controller = "Home", action = "Register" });
                routes.MapRoute("RegisterUser", "RegisterUser", new { controller = "Home", action = "RegisterUser" });
                routes.MapRoute("Login", "Login", new { controller = "Home", action = "Login" });
                routes.MapRoute("Logout", "Logout", new { controller = "Home", action = "Logout" });
                routes.MapRoute("searchResult", "searchResult", new { controller = "Series", action = "searchResult" });
                routes.MapRoute("FBLogin", "FBLogin", new { controller = "Home", action = "FBLogin" });
                routes.MapRoute("members", "members", new { controller = "Home", action = "members" });
                routes.MapRoute("addFriend", "addFriend", new { controller = "Home", action = "addFriend" });
                routes.MapRoute("Profile", "Profile", new { controller = "Home", action = "Profile" });
                routes.MapRoute("UpdateProfile", "UpdateProfile", new { controller = "Home", action = "UpdateProfile" });
                routes.MapRoute("UploadPicture", "UploadPicture", new { controller = "Home", action = "UploadPicture" });

                routes.MapRoute("title", "title/{id}", new { controller = "Series", action = "title" });
                routes.MapRoute("genre", "genre/{id}", new { controller = "Series", action = "genre" });
                routes.MapRoute("country", "country/{id}", new { controller = "Series", action = "country" });
                routes.MapRoute("creation", "creation/{id}", new { controller = "Series", action = "creation" });
                routes.MapRoute("actor", "actor/{id}", new { controller = "Series", action = "actor" });
                routes.MapRoute("network", "network/{id}", new { controller = "Series", action = "network" });
                routes.MapRoute("language", "language/{id}", new { controller = "Series", action = "language" });
                routes.MapRoute("education", "education/{id}", new { controller = "Series", action = "education" });
                routes.MapRoute("award", "award/{id}", new { controller = "Series", action = "award" });
                routes.MapRoute("addComment", "addComment", new { controller = "Series", action = "addComment" });
                routes.MapRoute("resultPagination", "resultPagination", new { controller = "Series", action = "resultPagination" });
                routes.MapRoute("addFavorites", "addFavorites", new { controller = "Series", action = "addFavorites" });
                routes.MapRoute("Favorites", "Favorites", new { controller = "Series", action = "Favorites" });
                routes.MapRoute("Rating", "Rating", new { controller = "Series", action = "Rating" });
                routes.MapRoute("top", "top", new { controller = "Series", action = "top" });
                routes.MapRoute("topseries", "topseries/{type}/{id}", new { controller = "Series", action = "topseries" });
                routes.MapRoute("Season", "Season", new { controller = "Series", action = "Season" });
                routes.MapRoute("Watched", "Watched", new { controller = "Series", action = "Watched" });
                routes.MapRoute("Episodes", "Episodes", new { controller = "Series", action = "Episodes" });

                routes.MapRoute("genres", "genres", new { controller = "Categories", action = "genres" });
                routes.MapRoute("countries", "countries", new { controller = "Categories", action = "countries" });
                routes.MapRoute("languages", "languages", new { controller = "Categories", action = "languages" });
                routes.MapRoute("networks", "networks", new { controller = "Categories", action = "networks" });
                routes.MapRoute("educations", "educations", new { controller = "Categories", action = "educations" });




                // routes.MapRoute("genre", "genre/{id}", new { controller = "Home", action = "genre" });
            });
           
        }
    }
}
