﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TVSeries.Models
{
    public class Models
    {
    }

    public class Series
    {
        public string title;
        public string description;
        public string img;
        public Dictionary<int, string> genres = new Dictionary<int, string>();
        public string genre_id;
        public int my_rating;
        public string favorites;
        public string total_rating;

        public Series(string title,string description,string img, Dictionary<int, string> genres,string genre_id,int my_rating,string favorites,string total_rating)
        {
            this.title = title;
            this.description = description;
            this.img = img;
            this.genres = genres;
            this.genre_id = genre_id;
            this.my_rating = my_rating;
            this.favorites = favorites;
            this.total_rating = total_rating;
        }


    }
    public class similar
    {
        public string img;
        public string title;

        public similar(string img,string title)
        {
            this.img = img;
            this.title = title;
        }
    }

    public class Person
    {
        public string name;
        public string img;
        public string description;
        public string subtitle;
        public int type; //0- actor, 2-composer, 1-creator

        public Person(string name,string img,string description,string subtitle,int type)
        {
            this.name = name;
            this.img = img;
            this.description = description;
            this.subtitle = subtitle;
            this.type = type;
        }
    }

    public class Comment
    {
        public string comment;
        public string date;
        public string username;
        public string img;

        public Comment(string comment, string date,string username,string img)
        {
            this.comment = comment;
            this.date = date;
            this.username = username;
            this.img = img;
        }
    }

    public class User
    {
        public string name;
        public string gender;
        public string country;
        public DateTime joined;
        public int isFriend;

        public User(string name,string gender,string country,DateTime joined,int isFriend)
        {
            this.name = name;
            this.gender = gender;
            this.country = country;
            this.joined = joined;
            this.isFriend = isFriend;
        }

    }

    public class TopSerie
    {
        public string title;
        public string img;
        public double rating;
        public int my_rating;
        public string favorites;
        public int nr_votes;

        public TopSerie(string title,string img,double rating, int my_rating,string favorites,int nr_votes)
        {
            this.title = title;
            this.img = img;
            this.rating = rating;
            this.my_rating = my_rating;
            this.favorites = favorites;
            this.nr_votes = nr_votes;
        }
    }

    public class Episode
    {
        public string name;
        public int number;
        public string airdate;
        public string img;
        public string summary;
        public int watched;

        public Episode(string name,int number,string airdate, string img,string summary,int watched)
        {
            this.name = name;
            this.number = number;
            this.airdate = airdate;
            this.img = img;
            this.summary = summary;
            this.watched = watched;
        }
    }

    public class WeekEpisodes
    {
        public string img;
        public string title;
        public string number;

        public WeekEpisodes(string img,string title,string number)
        {
            this.img = img;
            this.title = title;
            this.number = number;
        }
    }
}
